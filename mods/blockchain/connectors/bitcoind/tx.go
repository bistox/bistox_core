package bitcoind

import (
	"encoding/hex"
	"fmt"
	"gitlab.com/bistox/bistox_core/etc"
	"gitlab.com/bistox/bistox_core/mods/db/models"
)


type BitcoinTransaction struct {
	// BTC
	TxId string
	TxIdRaw []uint8
	IsCoinbase bool
	Outputs []*models.Utxo
	UsedTxId [][]uint8
	UsedVout []uint8
}


func (ct BitcoinTransaction) String() string {
	inputs, outputs := "", ""
	for i, input := range ct.UsedTxId {
		inputs += fmt.Sprintf("txid:%s vout:%d, ", hex.EncodeToString(*etc.ReverseBytes(&input)), ct.UsedVout[i])
	}
	for _, output := range ct.Outputs {
		outputs += output.String() + ", "
	}
	return fmt.Sprintf("\nInputs: [%v]\nOutputs: [%v]", inputs, outputs)
}