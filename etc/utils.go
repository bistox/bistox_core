package etc

import (
	"github.com/op/go-logging"
	"os"
)


func FileExists(name string) bool {
	if _, err := os.Stat(name); err != nil {
		if os.IsNotExist(err) {
			return false
		}
	}
	return true
}


func SetupLogger(verbosity logging.Level) {
	formatStdOut := logging.MustStringFormatter(
		`%{color}%{time:15:04:05.000} - %{level:.5s} - %{shortpkg} - %{shortfunc}:%{color:reset} %{message}`,
	)
	stdoutLogger := logging.AddModuleLevel(logging.NewBackendFormatter(logging.NewLogBackend(os.Stdout, "", 0), formatStdOut))
	stdoutLogger.SetLevel(verbosity, "")
	logging.SetBackend([]logging.Backend{stdoutLogger}...)
}


func ReverseBytes(bytes *[]byte) *[]byte {
	for start, end := 0, len(*bytes) - 1; start < end; start, end = start + 1, end - 1 {
		(*bytes)[start], (*bytes)[end] = (*bytes)[end], (*bytes)[start]
	}
	return bytes
}
