package connectors

import (
	"gitlab.com/bistox/bistox_core/etc"
	"gitlab.com/bistox/bistox_core/mods/db/models"
)


type BlockchainConfig struct {
	Currency models.CryptoCurrency
	Addr string
	RpcUser string
	RpcPass string
	Net string
	SyncLoopDelay int
	LastSyncedBlockHash string // in config?
	MinConfirmations int
	TxBroadcastTime int
}


func (c *BlockchainConfig) Validate() error {
	if c.Net == "" {
		return etc.Error(etc.InvalidConfig, "Net must be specified")
	}
	if c.TxBroadcastTime <= 0 {
		return etc.Error(etc.InvalidConfig, "Tx broadcast time is less or equal zero")
	}
	return nil
}
