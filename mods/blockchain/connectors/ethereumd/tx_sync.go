package ethereumd

import (
	"gitlab.com/bistox/bistox_core/mods/blockchain/connectors"
	"gitlab.com/bistox/bistox_core/mods/db/models"
	"strings"
)


//t *models.CryptoTransactionVerbose
func (c *Connector) SyncTransaction(tx connectors.BlockchainStorage, t connectors.Tx,
		payments *[]*models.Payment) error {

	if t.Data == nil {
		return nil
	}

	et := t.Data.(EthereumTransaction)

	to, from := strings.ToLower(et.To.Hex()), strings.ToLower(et.From.Hex())

	if c.cache.CheckAddress(to) {
		address, err := c.cache.GetAddress(to)

		if err != nil {
			return err
		}

		*payments = append(*payments, &models.Payment{
			Amount: et.Amount - et.Fee,
			WalletId: address.WalletId,
			Direction: models.DirectionIncoming,
			Status: models.StatusPending,
			Fee: 0,
			CryptoPayments: []*models.CryptoPayment{
				{
					AddressId: address.ID,
					Address: &models.CryptoAddress{
						Address: address.Address,
					},
					Amount: et.Amount - et.Fee,
				},
			},
		})
	}

	if !et.IsCoinbase && c.cache.CheckAddress(from) {
		address, err := c.cache.GetAddress(from)

		if err != nil {
			return err
		}

		*payments = append(*payments, &models.Payment{
			Amount: et.Amount,
			WalletId: address.WalletId,
			Direction: models.DirectionOutgoing,
			Status: models.StatusPending,
			Fee: et.Fee,
			CryptoPayments: []*models.CryptoPayment{
				{
					AddressId: address.ID,
					Address: &models.CryptoAddress{
						Address: address.Address,
					},
					Amount: et.Amount,
				},
			},
		})
	}

	return nil
}
