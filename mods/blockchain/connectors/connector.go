package connectors

import (
	"context"
	"gitlab.com/bistox/bistox_core/mods/db/models"
)


type BlockchainStorage interface {
	GetBlock(withLock bool) (*models.CryptoBlock, error)
	UpdateBlock(*models.CryptoBlock) error

	PaymentsAtHeight(height uint32) ([]models.Payment, error)
	PreparingPayments() ([]models.Payment, error)
	// Preparing -> Waiting
	PreparePayment(payment *models.Payment) error
	SetPaymentStatus(payment *models.Payment) error
	CheckPaymentsByTx(txId []byte, blockHeight uint32) (exist bool, err error)

	UpdateWalletBalance(walletId string, amount int64) error

	SpendTxInput(utxo *models.Utxo) (address string, amount int64, err error)

	GetWallets(wallets *[]*models.Wallet, limit, offset int, walletTypes ...models.WalletType) error

	UpdateAddressBalance(addressId uint64, amount int64, confirmed bool) error
	IncrementAddressNonce(addressId uint64) (nonce uint64, err error)

	GetCryptoTx(txId []byte) (*models.CryptoTransaction, error)
	GetUtxos(utxos *[]*models.Utxo, limit, offset int, height uint32, beforeHeight bool) error
	GetBestUtxoForUpdate(notIn []uint64, beforeHeight uint32) (*models.Utxo, error)

	Insert(interface{}) error

	PrivateKey(address string) ([]byte, error)

	RunInTransaction(func(tx BlockchainStorage) error) error
	BeginTransaction() (BlockchainStorage, error)
	RollbackTransaction() error
	CommitTransaction() error

	Notify(channel string, message string) error
	Listen(channel... string) EventListener
}


type Event struct {
	Channel string
	Payload string
}


type EventListener interface {
	Channel() <-chan *Event
	Close()
}


// Distributed Lock
type Lock interface {
	Release(context.Context)
}


type BlockchainCache interface {
	ObtainLock(key string) (Lock, error)

	InsertUtxo(amount float64, txCompact string) error
	DeleteUtxo(txCompact string) error

	RotateWallet(walletId string) (address string)
	ExchangeBestAddress() (address string, balance int64, err error)
	ExchangeWorstAddress() (address string, balance int64, err error)
	PositiveDepositAddress() (address string, balance int64, err error)

	InsertAddress(*models.CryptoAddress) error
	GetAddress(address string) (*models.CryptoAddress, error)
	// Danger
	CheckAddress(address string) bool
	UpdateAddressBalance(address string, amount int64) error

	InsertWallet(*models.Wallet) error
	UpdateWalletBalance(walletId string, amount int64) error

	LockMaxTxInput() (exists bool, txCompact string, amount int64)
}


type BlockchainConnector interface {
	BestBlock() (height uint32, hash string, err error)

	ValidateConfiguration() error

	CreateAddress() (privateKey []byte, publicKey []byte, address string, err error)

	InsertAddress(address *models.CryptoAddress) error

	MinConfirmations() int

	ConfirmedBalance(address string) (int64, error)

	PendingBalance(address string) (int64, error)

	PendingTransactions(address string) ([]*models.Payment, error)

	PaymentChannel() chan<- *models.Payment

	ValidateAddress(address string) error

	EstimateFee(amount int64) (int64, error)

	Currency() models.CryptoCurrency

	ConfirmedHeight() uint32
}


type Tx struct {
	TxId []byte
	Data interface{}
}


type BlockchainConnectorInner interface {
	Install(context.Context, BlockchainStorage, BlockchainCache)

	SyncNextBlock(prevHash string) (string, []Tx, error)
	SyncTransaction(tx BlockchainStorage, ti Tx, payments *[]*models.Payment) error
	GetSyncedHeight() uint32
	SetSyncedHeight(uint32)
	// Goroutine that accumulates and sends transactions to the network
	TxBuilder()

	WarmUpCache(full bool) error
}