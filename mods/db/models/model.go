package models

import (
	"fmt"
	"github.com/go-pg/pg"
	"github.com/go-pg/pg/orm"
	"strings"
	"time"
	"context"
)


type BaseModel struct {
	ID string               `sql:",pk,type:uuid,default:gen_random_uuid()"`
	CreatedAt time.Time		`sql:",notnull"`
	UpdatedAt time.Time		`sql:",notnull"`
	DeletedAt *time.Time
}


func (m *BaseModel) BeforeInsert(c context.Context, db orm.DB) error {
	now := time.Now()
	if m.CreatedAt.IsZero() {
		m.CreatedAt = now
	}
	if m.UpdatedAt.IsZero() {
		m.UpdatedAt = now
	}
	return nil
}


func (m *BaseModel) BeforeUpdate(c context.Context, db orm.DB) error {
	m.UpdatedAt = time.Now()
	return nil
}


func CreateIndexQuery(unique bool, columns ...string) string {
	uniqueStr := ""
	if unique {
		uniqueStr = "unique"
	}
	query := `create %s index concurrently if not exists "?TableName-%s_idx" ON ?TableName(%s)`
	return fmt.Sprintf(
		query,
		uniqueStr,
		strings.Join(columns, "_"),
		strings.Join(columns, ","),
	)
}


func CreateEnum(db *pg.DB, force bool, name string, columns ...string) error {
	exists := false

	if _, err := db.Query(&exists, "select exists (select true from pg_type where typname = ?);", name); err != nil {
		return err
	}

	if exists && force {
		if _, err := db.Exec(fmt.Sprintf("drop type %s cascade;", name)); err != nil {
			return err
		}
		exists = false
	}

	if exists {
		return nil
	}

	cols := make([]string, len(columns))
	for i, col := range columns {
		cols[i] = "'" + col + "'"
	}

	if _, err := db.Exec(fmt.Sprintf("create type %s as enum (%s)", name, strings.Join(cols, ","))); err != nil {
		return err
	}

	return nil
}
