package bitcoind

import (
	"fmt"
	"gitlab.com/bistox/bistox_core/mods/blockchain/connectors"
	"gitlab.com/bistox/bistox_core/mods/db/models"
)


func (c *Connector) SyncTransaction(tx connectors.BlockchainStorage, ti connectors.Tx, payments *[]*models.Payment) error {
	t := ti.Data.(BitcoinTransaction)
	inputAddr, outputAddr := map[string]int64{}, map[string]int64{}
	var inputTotal, outputTotal int64 = 0, 0

	for _, o := range t.Outputs {
		if o == nil {
			continue
		}

		if !c.cache.CheckAddress(o.Address.Address) {
			continue
		}

		addr, err := c.cache.GetAddress(o.Address.Address)

		if err != nil {
			return err
		}

		outputAddr[o.Address.Address] += o.Amount
		outputTotal += o.Amount
		o.AddressId = addr.ID

		if err := tx.Insert(o); err != nil {
			log.Error(err)
			return err
		}
		msg := fmt.Sprintf("%s %d %s", o.Address, o.Amount, o.CompactTxOutString())
		if err := tx.Notify("new_tx", msg); err != nil {
			return err
		}
	}

	for i, usedTx := range t.UsedTxId {
		vout := t.UsedVout[i]
		ct := &models.Utxo{TxId: usedTx, Vout: vout}
		address, amount, err := c.storage.SpendTxInput(ct)

		if err != nil {
			log.Error(err)
			return err
		}

		if address == "" { continue }

		inputAddr[address] += amount
		inputTotal += amount

		msg := fmt.Sprintf("%s %s", address, ct.CompactTxOutString())
		if err := tx.Notify("del_tx", msg); err != nil {
			return err
		}
	}

	if t.IsCoinbase {
		for addr, amount := range outputAddr {
			if !c.cache.CheckAddress(addr) {
				continue
			}

			data, err := c.cache.GetAddress(addr)

			if err != nil {
				return err
			}

			*payments = append(*payments, &models.Payment{
				//TxId: t.TxIdRaw,
				Amount: amount,
				WalletId: data.WalletId,
				Status: models.StatusPending,
				Direction: models.DirectionIncoming,
				CryptoPayments: []*models.CryptoPayment{
					{
						AddressId: data.ID,
						Amount: amount,
					},
				},
			})
		}
	} else {
		var fee int64
		if len(inputAddr) > 0 {
			fee = (inputTotal - outputTotal) / int64(len(inputAddr))
		}
		for addr, outAmount := range outputAddr {
			if !c.cache.CheckAddress(addr) {
				continue
			}

			data, err := c.cache.GetAddress(addr)

			if err != nil {
				return err
			}

			p := &models.Payment{
				//TxId: t.TxIdRaw,
				WalletId: data.WalletId,
				Amount: outAmount,
				Status: models.StatusPending,
				Direction: models.DirectionIncoming,
				CryptoPayments: []*models.CryptoPayment{
					{
						AddressId: data.ID,
						Amount: outAmount,
					},
				},
			}
			if inAmount, ok := inputAddr[addr]; ok {
				p.Fee = fee
				p.Amount = inAmount - outAmount
				p.CryptoPayments[0].Amount = p.Amount

				if outAmount < inAmount {
					p.Direction = models.DirectionOutgoing
				}
			}
			*payments = append(*payments, p)
		}
		for addr, amount := range inputAddr {
			if _, ok := outputAddr[addr]; ok {
				continue
			}
			if !c.cache.CheckAddress(addr) {
				continue
			}

			data, err := c.cache.GetAddress(addr)

			if err != nil {
				return err
			}

			*payments = append(*payments, &models.Payment{
				//TxId: t.TxIdRaw,
				Amount: amount,
				WalletId: data.WalletId,
				Fee: fee,
				Status: models.StatusPending,
				Direction: models.DirectionOutgoing,
				CryptoPayments: []*models.CryptoPayment{
					{
						AddressId: data.ID,
						Amount: amount,
					},
				},
			})
		}
	}
	return nil
}
