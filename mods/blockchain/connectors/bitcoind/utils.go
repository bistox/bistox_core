package bitcoind

import (
	"github.com/btcsuite/btcd/btcec"
	"github.com/btcsuite/btcd/chaincfg"
	"github.com/btcsuite/btcutil"
	"gitlab.com/bistox/bistox_core/etc"
	"gitlab.com/bistox/bistox_core/mods/blockchain/currencies/bitcoin"
	"gitlab.com/bistox/bistox_core/mods/db/models"
)


const Satoshi = 1
const Bitcoin = 100000000 * Satoshi


func getParams(currency models.CryptoCurrency, network string) (*chaincfg.Params, error) {
	switch currency {
	case models.BTC:
		return bitcoin.GetParams(network)
	default:
		return nil, etc.InternalErrorf("Unsupported currency(%v)", currency)
	}
}


func toSatoshi(amount float64) int64 {
	return int64(amount * Bitcoin)
}


func NewAddress(params *chaincfg.Params) (privateKeyBytes []byte, publicKeyBytes []byte, address string, err error) {
	privateKey, err := btcec.NewPrivateKey(btcec.S256())
	if err != nil {
		return nil, nil, "", err
	}
	pubKey := privateKey.PubKey()
	pubKeyHash := btcutil.Hash160(pubKey.SerializeCompressed())
	addr, err := btcutil.NewAddressPubKeyHash(pubKeyHash, params)
	return privateKey.Serialize(), pubKey.SerializeCompressed(), addr.EncodeAddress(), nil
}
