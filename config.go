package bistox_core

import (
	"errors"
	"gitlab.com/bistox/bistox_core/mods/blockchain"
	"gitlab.com/bistox/bistox_core/mods/cache"
	"gitlab.com/bistox/bistox_core/mods/db"
	"gitlab.com/bistox/bistox_core/mods/server"
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"os"
)


const (
	configEnvName = "BISTOX_CONFIG"
	defaultLogLevel = "info"
	defaultConfigFilename = "bistox.yaml"
)


type Config struct {
	DebugLevel string
	Ext map[interface{}]interface{}

	Db *db.Config
	Server *server.Config
    Blockchain *blockchain.Config
	Cache *cache.Config
}


func getDefaultConfig() Config {
	return Config {
		DebugLevel: defaultLogLevel,
		Ext: map[interface{}]interface{}{},
		Db: db.GetDefaultConfig(),
		Server: server.GetDefaultConfig(),
		Blockchain: blockchain.GetDefaultConfig(),
		Cache: cache.GetDefaultConfig(),
	}
}


func (c *Config) loadConfig() error {
	var data []byte
	var err error

	data = []byte(os.Getenv(configEnvName))

	if len(data) == 0 {
		if data, err = ioutil.ReadFile(defaultConfigFilename); err != nil {
			log.Fatalf("Error reading config file: %s", defaultConfigFilename)
			return err
		}
	}

	if err = yaml.Unmarshal(data, &c); err != nil {
		log.Fatalf("Error parsing yaml: %v", err)
		return err
	}

	return nil
}


func (c *Config) Get(path ...string) interface{} {
	type m = map[interface{}]interface{}
	pathLen := len(path)
	var tmp = c.Ext
	for i, p := range path {
		val, ok := tmp[p]
		if !ok {
			return nil
		}
		switch val.(type) {
		case m:
			tmp = val.(m)
		default:
			if i == pathLen - 1 {
				return val
			} else {
				return nil
			}
		}
	}
	return tmp
}


func (c *Config) GetInt64(path ...string) (int64, error) {
	val := c.Get(path...)
	switch val.(type) {
	case int:
		return int64(val.(int)), nil
	case int32:
		return int64(val.(int32)), nil
	case int64:
		return val.(int64), nil
	}
	return 0, errors.New("can't get int64 value from the config")
}


func (c *Config) GetString(path ...string) string {
	return c.Get(path...).(string)
}


func (c *Config) GetMap(path ...string) map[interface{}]interface{} {
	return c.Get(path...).(map[interface{}]interface{})
}