package cache

import (
	"github.com/go-redis/redis"
	"github.com/op/go-logging"
	"gitlab.com/bistox/bistox_core/etc"
	"sync"
	"time"
)

var log = logging.MustGetLogger("bistox")

const HealthCheckInterval = 10 * time.Second
const KeyName = "cache"


type Mod struct {
	Cache *redis.Client
	config *Config
}


type Config struct {
	Enabled bool
	Options *redis.Options
}


func GetDefaultConfig() *Config {
	return &Config{
		Enabled: false,
		Options: &redis.Options{
			Addr: "localhost:6379",
			Password: "",
			PoolSize: 20,
			DB: 0,
		},
	}
}


func (mod Mod) Cleanup(ready *sync.WaitGroup) {
	mod.Cache.Close()
	log.Info("Disconnected from Redis")
	ready.Done()
}


func (Mod) KeyName() string {
	return KeyName
}


func (mod Mod) Enabled() bool {
	return mod.config.Enabled
}


func (mod Mod) Run(ready *sync.WaitGroup, quit chan<- bool) {

	if healthCheck(mod.Cache) == nil {
		log.Info("Connected to Redis!")
	}

	ready.Done()

	for {
		if err := healthCheck(mod.Cache); err != nil {
			log.Error(err)
			quit <- true
		}
		time.Sleep(HealthCheckInterval)
	}
}


func healthCheck(redis *redis.Client) error {
	if res, err := redis.Ping().Result(); err != nil || res != "PONG" {
		return etc.InternalErrorf("Redis error %v %v", err, res)
	}
	return nil
}


func New(config *Config) Mod {
	cache := redis.NewClient(config.Options)
	return Mod{cache, config}
}
