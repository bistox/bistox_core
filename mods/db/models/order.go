package models

import (
	"github.com/go-pg/pg"
	"github.com/golang/protobuf/ptypes"
	"github.com/golang/protobuf/ptypes/timestamp"
	pb "gitlab.com/bistox/bistox_proto"
	"time"
)

type OrderType string
const (
	Market OrderType = "MARKET"
	Limit  OrderType = "LIMIT"
)

func (orderType OrderType) ToProto() pb.OrderType {
	return pb.OrderType(pb.OrderType_value[string(orderType)])
}

func ProtoToOrderType(protoOrderType pb.OrderType) OrderType {
	return OrderType(protoOrderType.String())
}

type OperationType string
const (
	Buy  OperationType = "BUY"
	Sell OperationType = "SELL"
)

func (opType OperationType) ToProto() pb.OperationType {
	return pb.OperationType(pb.OperationType_value[string(opType)])
}

func ProtoToOperationType(protoOpType pb.OperationType) OperationType {
	return OperationType(protoOpType.String())
}

type Order struct {
	BaseModel
	ID uint64							`sql:",pk"`
	tableName 			struct{}        `sql:"orders,alias:o"`
	OrderType          	OrderType		`sql:",type:order_type,notnull"`
	OperationType      	OperationType	`sql:",type:operation_type,notnull"`
	BaseCurrencyCode   	CurrencyEnum	`sql:",type:currency_code,notnull,"`
	QuotedCurrencyCode 	CurrencyEnum	`sql:",type:currency_code,notnull,"`
	Amount             	int64			`sql:"default:0,notnull"`
	Price              	int64			`sql:"default:0,notnull"`
	BaseWallet         	*Wallet			`pg:"fk:base_wallet_id"`
	BaseWalletId       	string			`sql:",notnull,type:uuid"`
	QuotedWallet       	*Wallet			`pg:"fk:quoted_wallet_id"`
	QuotedWalletId     	string			`sql:",notnull,type:uuid"`
	Profile				*Profile		`pg:"fk:profile_id"`
	ProfileId			string			`sql:",notnull,type:uuid"`
	FilledAmount       	int64			`sql:"default:0,notnull"`
	FilledPrice        	int64			`sql:"default:0,notnull"`
	Canceled           	bool			`sql:"default:false,notnull"`
	Failed             	bool			`sql:"default:false,notnull"`
}

func (o *Order) Prepare(db *pg.DB, force bool) error {
	if err := CreateEnum(
		db, force, "order_type",
		string(Market), string(Limit),
	); err != nil {return  err}

	if err := CreateEnum(
		db, force, "operation_type",
		string(Buy), string(Sell),
	); err != nil {return  err}

	return nil
}


func (o *Order) Sync(db *pg.DB, force bool) error {
	db.Model((*Order)(nil)).Exec(CreateIndexQuery(false,"profile_id"))
	db.Model((*Order)(nil)).Exec(CreateIndexQuery(false,"base_wallet_id"))
	db.Model((*Order)(nil)).Exec(CreateIndexQuery(false,"quoted_wallet_id"))
	db.Model((*Order)(nil)).Exec(CreateIndexQuery(false,"base_currency_code"))
	db.Model((*Order)(nil)).Exec(CreateIndexQuery(false,"quoted_currency_code"))
	db.Model((*Order)(nil)).Exec(CreateIndexQuery(false,"order_type"))
	db.Model((*Order)(nil)).Exec(CreateIndexQuery(false,"operation_type"))
	db.Model((*Order)(nil)).Exec(CreateIndexQuery(
		false,
		"base_currency_code",
		"quoted_currency_code",
		"order_type",
		"operation_type",
		"deleted_at",
	))
	db.Model((*Order)(nil)).Exec(CreateIndexQuery(
		false,
		"base_currency_code",
		"quoted_currency_code",
		"deleted_at",
	))
	return nil
}

func (o *Order) Left() int64 {
	return o.Amount - o.FilledAmount
}

func (o *Order) Invalid() bool {
	return o.Canceled || o.Failed
}

func (o *Order) ToProto() *pb.Order {
	createdAt, _ := ptypes.TimestampProto(o.CreatedAt)
	var deletedAt *timestamp.Timestamp
	if o.DeletedAt != nil {
		deletedAt, _ = ptypes.TimestampProto(*o.DeletedAt)
	} else {
		deletedAt, _ = ptypes.TimestampProto(time.Time{})
	}

	return &pb.Order{
		OrderId: o.ID,
		OrderType: o.OrderType.ToProto(),
		OperationType: o.OperationType.ToProto(),
		BaseCurrency: o.BaseCurrencyCode.ToProto(),
		QuotedCurrency: o.QuotedCurrencyCode.ToProto(),
		Amount: float64(o.Amount) / float64(Divisor[o.BaseCurrencyCode]),
		Price: float64(o.Price) / float64(Divisor[o.QuotedCurrencyCode]),
		FilledAmount: float64(o.FilledAmount) / float64(Divisor[o.BaseCurrencyCode]),
		FilledPrice: float64(o.FilledPrice) / float64(Divisor[o.QuotedCurrencyCode]),
		Canceled: o.Canceled,
		Failed: o.Failed,
		CreatedAt: createdAt,
		DeletedAt: deletedAt,
	}
}