package bitcoind

import (
	"context"
	"github.com/btcsuite/btcd/btcec"
	"github.com/btcsuite/btcd/btcjson"
	"github.com/btcsuite/btcd/chaincfg"
	"github.com/btcsuite/btcd/chaincfg/chainhash"
	"github.com/btcsuite/btcd/rpcclient"
	"github.com/btcsuite/btcd/wire"
	"github.com/btcsuite/btcutil"
	"github.com/op/go-logging"
	"gitlab.com/bistox/bistox_core/etc"
	"gitlab.com/bistox/bistox_core/mods/blockchain/connectors"
	"gitlab.com/bistox/bistox_core/mods/db/models"
)

var log = logging.MustGetLogger("bistox")

type Config = connectors.BlockchainConfig


type Connector struct {
	ctx context.Context
	config *Config
	client *rpcclient.Client
	storage connectors.BlockchainStorage
	cache connectors.BlockchainCache

	netParams *chaincfg.Params
	bestBlockHeight uint32
	paymentChan chan *models.Payment

	currentTx *rawTransaction
	currentDbTx connectors.BlockchainStorage
}


type rawTransaction struct {
	msgTx *wire.MsgTx
	fee int64
	totalInAmount int64
	totalOutAmount int64
}


// Force check interface implementation
var _ connectors.BlockchainConnector = (*Connector)(nil)
var _ connectors.BlockchainConnectorInner = (*Connector)(nil)


func NewConnector(config *Config) connectors.BlockchainConnector {
	rpcConfig := &rpcclient.ConnConfig{
		Host: config.Addr,
		User: config.RpcUser,
		Pass: config.RpcPass,
		DisableTLS: true,
		HTTPPostMode: true,
	}

	client, _ := rpcclient.New(rpcConfig, nil)
	connector := Connector{
		config: config,
		client: client,
		netParams: &chaincfg.TestNet3Params,
		paymentChan: make(chan *models.Payment),
	}

	return &connector
}


func (c *Connector) ValidateConfiguration() error {
	if err := c.config.Validate(); err != nil {
		log.Critical(err)
		return err
	}

	resp, err := c.client.GetBlockChainInfo()

	if err != nil {
		return etc.InternalErrorf( "Unable to get type of network: %v", err)
	}
	chain := resp.Chain

	if c.config.Net != chain {
		return etc.InternalErrorf("Networks are different, desired: %v, actual: %v", c.config.Net, chain)
	}

	log.Infof("Init connector working with '%v' net", c.config.Net)

	c.netParams, err = getParams(c.config.Currency, chain)
	if err != nil {
		return etc.InternalErrorf("Failed to get net params: %v", err)
	}

	return nil
}


func (c *Connector) syncUnconfirmed() error {
	_, err := c.client.ListUnspentMinMax(0, int(c.config.MinConfirmations - 1))
	if err != nil {
		return err
	}

	return nil
}


func (c *Connector) findForkBlock(orphanBlock *btcjson.GetBlockVerboseResult) (*btcjson.GetBlockVerboseResult, error) {
	for orphanBlock.Confirmations == -1 {
		prevHash, err := chainhash.NewHashFromStr(orphanBlock.PreviousHash)
		if err != nil {
			return nil, etc.InternalErrorf("Unable to decode hash of prev orphan block: %v", err)
		}

		orphanBlock, err = c.client.GetBlockVerbose(prevHash)
		if err != nil {
			return nil, etc.InternalErrorf("Unable to prev last sync block from daemon: %v", err)
		}
	}

	return orphanBlock, nil
}


func (c *Connector) ConfirmedBalance(account string) (int64, error) {
	balance, err := c.client.GetBalanceMinConf(account, c.config.MinConfirmations)
	if err != nil {
		return 0, err
	}

	return int64(balance.ToBTC()), nil
}


func (c *Connector) CreateAddress() ([]byte, []byte, string, error) {
	privateKey, err := btcec.NewPrivateKey(btcec.S256())
	if err != nil {
		return nil, nil, "", err
	}
	pubKey := privateKey.PubKey()
	pubKeyHash := btcutil.Hash160(pubKey.SerializeCompressed())
	addr, err := btcutil.NewAddressPubKeyHash(pubKeyHash, c.netParams)
	return privateKey.Serialize(), pubKey.SerializeCompressed(), addr.EncodeAddress(), nil
}


func (c *Connector) InsertAddress(address *models.CryptoAddress) error {
	return c.cache.InsertAddress(address)
}


func (c *Connector) PendingBalance(address string) (int64, error) {
	if address, err := c.cache.GetAddress(address); err != nil {
		return 0, err
	} else {
		return address.Balance, nil
	}
	return 0, nil
}


func (c *Connector) PendingTransactions(account string) ([]*models.Payment, error) {
	return []*models.Payment{}, nil
}


func (c *Connector) PaymentChannel() chan<- *models.Payment {
	return c.paymentChan
}


func (c *Connector) ValidateAddress(address string) error {
	_, err := btcutil.DecodeAddress(address, c.netParams)
	return err
}


func (c *Connector) EstimateFee(amount int64) (int64, error) {
	fee, err := c.client.EstimateFee(amount)
	if err != nil {
		return 0, err
	}
	return int64(fee), nil
}


func (c *Connector) SyncNextBlock(prevHash string) (string, []connectors.Tx, error) {
	hash, _ := chainhash.NewHashFromStr(prevHash)
	prevBlock, err := c.client.GetBlockVerbose(hash)

	if err != nil {
		return "", nil, etc.InternalErrorf("Unable to process block: %v", err)
	}

	if prevBlock.NextHash == "" {
		return "", nil, etc.Error(etc.Ignored, "Next blockhash is empty")
	}

	hash, _ = chainhash.NewHashFromStr(prevBlock.NextHash)
	block, err := c.client.GetBlockVerbose(hash)

	// blockchain soft/hard fork detected!
	if block.Confirmations < 0 {
		log.Info("Chain reorganization has been found, handle it...")

		forkBlock, err := c.findForkBlock(block)
		if err != nil {
			return "", nil, etc.InternalErrorf("Unable to handle reorganizations: %v", err)
		}

		log.Infof("Fork has been detected on block(%v) using it as last synced block", forkBlock.Hash)
		block = forkBlock
	}

	transactions, err := c.handleBlock(block)

	if err != nil {
		return "", nil, etc.InternalErrorf("Unable to process block: %v", err)
	}

	return prevBlock.NextHash, transactions, nil
}


func (c *Connector) handleBlock(block *btcjson.GetBlockVerboseResult) ([]connectors.Tx, error) {
	transactions := make([]connectors.Tx, len(block.Tx))

	for iTx, txHashStr := range block.Tx {

		txHash, err := chainhash.NewHashFromStr(txHashStr)
		if err != nil {
			log.Errorf("Unable to decode tx hash(%v)", txHashStr)
			continue
		}

		tx, _ := c.client.GetRawTransactionVerbose(txHash)

		if err != nil {
			continue
		}

		transactionVerbose := BitcoinTransaction{
			TxId: `\x` + txHash.String(),
			TxIdRaw: txHash.CloneBytes(),
			Outputs: make([]*models.Utxo, len(tx.Vout)),
			UsedTxId: make([][]uint8, len(tx.Vin)),
			UsedVout: make([]uint8, len(tx.Vin)),
		}

		for i, vin := range tx.Vin {
			if vin.IsCoinBase() {
				transactionVerbose.UsedTxId = [][]uint8{}
				transactionVerbose.UsedVout = []uint8{}
				transactionVerbose.IsCoinbase = true
				break
			}
			h, _ := chainhash.NewHashFromStr(vin.Txid)
			transactionVerbose.UsedTxId[i] = h.CloneBytes()
			transactionVerbose.UsedVout[i] = uint8(vin.Vout)
		}

		for i, vout := range tx.Vout {
			// Ignore OpReturn, etc
			if len(vout.ScriptPubKey.Addresses) == 0 {
				continue
			}
			transaction := &models.Utxo{
				Address: &models.CryptoAddress{
					Address: vout.ScriptPubKey.Addresses[0],
				},
				Amount: toSatoshi(vout.Value),
				CurrencyCode: models.CurrencyEnum(c.config.Currency),
				TxId: txHash.CloneBytes(),
				BlockHeight: uint32(block.Height),
				Vout: uint8(i),
			}
			transactionVerbose.Outputs[i] = transaction
		}

		transactions[iTx] = connectors.Tx{
			TxId: transactionVerbose.TxIdRaw,
			Data: transactionVerbose,
		}

		log.Infof("Receive transaction %v", transactionVerbose)
	}

	return transactions, nil
}


func (c *Connector) BestBlock() (uint32, string, error) {
	hash, height, err := c.client.GetBestBlock()
	if err != nil {
		return 0, "", err
	}
	return uint32(height), hash.String(), nil
}


func (c *Connector) Payment() {
}


func (c *Connector) Currency() models.CryptoCurrency {
	return c.config.Currency
}


func (c *Connector) Install(ctx context.Context, storage connectors.BlockchainStorage, cache connectors.BlockchainCache) {
	c.ctx = ctx
	c.cache = cache
	c.storage = storage
}

func (c *Connector) GetSyncedHeight() uint32 {
	return c.bestBlockHeight
}


func (c *Connector) SetSyncedHeight(height uint32) {
	c.bestBlockHeight = height
}


func (c *Connector) MinConfirmations() int {
	return c.config.MinConfirmations
}


func (c *Connector) ConfirmedHeight() uint32 {
	if c.config.MinConfirmations >= int(c.bestBlockHeight) {
		return 0
	} else {
		return c.bestBlockHeight - uint32(c.config.MinConfirmations)
	}
}

