package etc

import "fmt"


type BistoxErrorType byte

const (
	DefaultError  BistoxErrorType = 1

	InvalidConfig  BistoxErrorType = 10
	ModuleDisabled BistoxErrorType = 11

	InsufficientBalance BistoxErrorType = 20
	MalformedArgs BistoxErrorType = 21
	NotFound BistoxErrorType = 22

	Ignored BistoxErrorType = 0
)

var errMap = map[BistoxErrorType]string {
	DefaultError: "InternalError",
	InvalidConfig: "InvalidConfig",
	ModuleDisabled: "ModuleDisabled",

	InsufficientBalance: "InsufficientBalance",
	MalformedArgs: "MalformedArgs",

	Ignored: "Ignored",
}


type BistoxError struct {
	BistoxErrorType
	Description string
}


func (be BistoxError) Error() string {
	err := fmt.Sprintf("%s: %s", errMap[be.BistoxErrorType], be.Description)
	return err
}


func Errorf(t BistoxErrorType, desc string, args ...interface{}) BistoxError {
	return BistoxError{t, fmt.Sprintf(desc, args...)}
}


func Error(t BistoxErrorType, desc string) BistoxError {
	return BistoxError{t, desc}
}


func InternalError(desc string) BistoxError {
	return BistoxError{DefaultError, desc}
}


func InternalErrorf(desc string, args ...interface{}) BistoxError {
	return BistoxError{DefaultError, fmt.Sprintf(desc, args...)}
}
