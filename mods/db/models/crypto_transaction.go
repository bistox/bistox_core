package models

import (
	"encoding/hex"
	"github.com/go-pg/pg"
)


type CryptoTransaction struct {
	TxId []byte     `sql:",pk"`
	Tx []byte		`sql:",notnul"`
}


func (CryptoTransaction) Prepare(db *pg.DB, force bool) error {
	return nil
}


func (CryptoTransaction) Sync(db *pg.DB, force bool) error {
	return nil
}


func (ct *CryptoTransaction) TxString() string {
	return hex.EncodeToString(ct.TxId)
}
