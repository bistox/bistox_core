package db

import (
	"github.com/go-pg/pg"
	"github.com/go-pg/pg/orm"
	"gitlab.com/bistox/bistox_core/mods/db/models"
)


type Model interface {
	Prepare(db *pg.DB, force bool) error
	Sync(db *pg.DB, force bool) error
}


func sync(db *pg.DB, force bool) error {
	tables := [] Model {
		&models.Profile{},
		&models.Token{},
		&models.Currency{},
		&models.Wallet{},
		&models.CryptoBlock{},
		&models.CryptoTransaction{},
		&models.CryptoAddress{},
		&models.Utxo{},
		&models.Payment{},
		&models.CryptoPayment{},
		&models.Order{},
		&models.Trade{},
		&models.TradeHistory{},
	}

	for _, t := range tables {
		if force {
			db.DropTable(t, &orm.DropTableOptions{Cascade:true, IfExists:true})
		}
		if err := t.Prepare(db, force); err != nil {
			return err
		}
		if err := db.CreateTable(t, &orm.CreateTableOptions{IfNotExists:true, FKConstraints:true}); err != nil {
			return err
		}
		if err := t.Sync(db, force); err != nil {
			return err
		}

		db.Model(t).Exec(models.CreateIndexQuery(false, "created_at"))
		db.Model(t).Exec(models.CreateIndexQuery(false, "deleted_at"))
	}

	return nil
}
