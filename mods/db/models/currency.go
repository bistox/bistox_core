package models

import (
	"github.com/go-pg/pg"
	pb "gitlab.com/bistox/bistox_proto"
)


type CurrencyEnum string

type CryptoCurrency = CurrencyEnum
var (
	BTC  CryptoCurrency = "BTC"
	BCH  CryptoCurrency = "BCH"
	ETH  CryptoCurrency = "ETH"
	LTC  CryptoCurrency = "LTC"
	DASH CryptoCurrency = "DASH"
)

type FiatCurrency = CurrencyEnum
var (
	USD FiatCurrency = "USD"
)

var Divisor = map[CurrencyEnum]int64{
	ETH: 1000000,
	BTC: 100000000,
	BCH: 100000000,
	LTC: 100000000,
    DASH:100000000,
	USD: 100,
}

func (currency CurrencyEnum) ToProto() pb.Currency {
	return pb.Currency(pb.Currency_value[string(currency)])
}

func ProtoToCurrencyEnum(protoCurrency pb.Currency) CurrencyEnum {
	return CurrencyEnum(protoCurrency.String())
}

var CryptoCurrencies = []CryptoCurrency{
	BTC,
	BCH,
	ETH,
	LTC,
	DASH,
}
var FiatCurrencies = []FiatCurrency{
	USD,
}

type Currency struct {
	tableName struct{} 	`sql:"currencies,alias:c"`
	IsoCode string		`sql:",pk,notnull,type:varchar(4),unique"`
	Crypto bool			`sql:",notnull,default:true,"`
}


func (c Currency) Prepare(db *pg.DB, force bool) error {
	if err := CreateEnum(
		db, force, "currency_code",
		string(BTC), string(BCH), string(ETH), string(LTC), string(DASH), string(USD),
	); err != nil {return  err}
	return nil
}


func (c Currency) Sync(db *pg.DB, force bool) error {
	for _, cc := range CryptoCurrencies {
		db.Insert(&Currency{IsoCode: string(cc), Crypto: true})
	}
	for _, fc := range FiatCurrencies {
		db.Insert(&Currency{IsoCode: string(fc), Crypto: false})
	}
	return nil
}

type CurrencyPair struct {
	BaseCurrency CurrencyEnum
	QuotedCurrency CurrencyEnum
}

var ValidPairs = []CurrencyPair{
	// Crypto-crypto pairs
	{
		BaseCurrency: ETH,
		QuotedCurrency: BTC,
	},
	{
		BaseCurrency: DASH,
		QuotedCurrency: BTC,
	},
	{
		BaseCurrency: LTC,
		QuotedCurrency: BTC,
	},
	{
		BaseCurrency: BCH,
		QuotedCurrency: BTC,
	},
	// Crypto-fiat pairs
	{
		BaseCurrency: BTC,
		QuotedCurrency: USD,
	},
	{
		BaseCurrency: LTC,
		QuotedCurrency: USD,
	},
	{
		BaseCurrency: BCH,
		QuotedCurrency: USD,
	},
	{
		BaseCurrency: ETH,
		QuotedCurrency: USD,
	},
	{
		BaseCurrency: DASH,
		QuotedCurrency: USD,
	},
}

func (pair CurrencyPair) IsValid() bool {
	for _, validPair := range ValidPairs {
		if pair.BaseCurrency == validPair.BaseCurrency &&
			pair.QuotedCurrency == validPair.QuotedCurrency {
			return true
		}
	}
	return false
}
