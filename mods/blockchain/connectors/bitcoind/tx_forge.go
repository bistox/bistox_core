package bitcoind

import (
	"bytes"
	"context"
	"fmt"
	"github.com/btcsuite/btcd/btcec"
	"github.com/btcsuite/btcd/chaincfg/chainhash"
	"github.com/btcsuite/btcd/txscript"
	"github.com/btcsuite/btcd/wire"
	"github.com/btcsuite/btcutil"
	"github.com/go-pg/pg"
	"gitlab.com/bistox/bistox_core/etc"
	"gitlab.com/bistox/bistox_core/mods/db/models"
	"time"
)


func (c *Connector) handlePayment(usedUtxos []uint64, p *models.Payment) error {
	amount := -p.Amount
	c.currentTx.totalOutAmount += amount

	for {
		// todo: check fee
		if c.currentTx.totalInAmount >= c.currentTx.totalOutAmount {
			break
		}

		height := c.ConfirmedHeight() - 101

		utxo, err := c.storage.GetBestUtxoForUpdate(usedUtxos, height)

		if err == pg.ErrNoRows {
			return etc.Error(etc.InsufficientBalance, "Exchange's treasury is empty")
		} else if err != nil {
			return err
		}

		// todo: check to_txid destination
		if _, _, err := c.currentDbTx.SpendTxInput(utxo); err != nil {
			return err
		}

		usedUtxos = append(usedUtxos, utxo.ID)
		c.currentTx.totalInAmount += utxo.Amount

		prevOutHash, _ := chainhash.NewHash(*etc.ReverseBytes(&utxo.TxId))
		prevOut := wire.NewOutPoint(prevOutHash, uint32(utxo.Vout))
		addr, _ := btcutil.DecodeAddress(utxo.Address.Address, c.netParams)
		addrPayScript, _ := txscript.PayToAddrScript(addr)
		txIn := wire.NewTxIn(prevOut, addrPayScript, nil)
		c.currentTx.msgTx.AddTxIn(txIn)
	}

	if c.currentTx.totalInAmount < amount {
		return etc.Error(etc.InsufficientBalance, "Exchange's treasury is empty")
	}

	//c.currentTx.totalOutAmount += amount

	toAddress, _ := btcutil.DecodeAddress(p.ExtAddress, c.netParams)
	toAddressPayScript, _ := txscript.PayToAddrScript(toAddress)
	c.currentTx.msgTx.AddTxOut(wire.NewTxOut(amount, toAddressPayScript))

	return nil
}


func (c *Connector) finalizeCryptoTx(payments []*models.Payment) error {
	if len(payments) == 0 {
		return etc.Error(etc.Ignored, "No payments")
	}

	// todo: read from config
	feePerByte, err := c.client.EstimateFee(10)
	feePerByte = 56000

	if err != nil {
		return err
	}

	log.Debugf("New transaction size: %d", c.currentTx.msgTx.SerializeSize())

	// check P2PKHOutputSize!
	fee := int64(feePerByte) * int64(c.currentTx.msgTx.SerializeSize() + P2PKHOutputSize)

	changeAmount := c.currentTx.totalInAmount - c.currentTx.totalOutAmount - fee
	c.currentTx.totalOutAmount += changeAmount

	changeAddress, _, err := c.cache.ExchangeWorstAddress()

	if err != nil {
		return err
	}

	btcChangeAddress, _ := btcutil.DecodeAddress(changeAddress, c.netParams)
	changeAddressPayScript, _ := txscript.PayToAddrScript(btcChangeAddress)
	c.currentTx.msgTx.AddTxOut(wire.NewTxOut(changeAmount, changeAddressPayScript))

	if err := c.signTx(); err != nil {
		return err
	}

	buf := bytes.NewBuffer(make([]byte, 0, c.currentTx.msgTx.SerializeSize()))
	c.currentTx.msgTx.Serialize(buf)
	txId := c.currentTx.msgTx.TxHash()

	if err := c.currentDbTx.Insert(&models.CryptoTransaction{
		TxId: txId.CloneBytes(),
		Tx: buf.Bytes(),
	}); err != nil {
		return err
	}

	for _, p := range payments {
		p.TxId = txId.CloneBytes()
		if err := c.currentDbTx.PreparePayment(p); err != nil {
			return err
		}
	}

	if err := c.currentDbTx.CommitTransaction(); err != nil {
		return err
	}

	log.Debug("Sending raw transaction...", c.currentTx.msgTx.TxHash().String())
	log.Debug(
		fmt.Sprintf("TotalIn: %d, TotalOut: %d, fee: %d",
			c.currentTx.totalInAmount,
			c.currentTx.totalOutAmount,
			c.currentTx.totalInAmount - c.currentTx.totalOutAmount,
		),
	)

	// send tx!
	if _, err = c.client.SendRawTransaction(c.currentTx.msgTx, false); err != nil {
		log.Error(err)
	}

	c.currentTx = nil

	return nil
}


// FIXME: read from db
func (c *Connector) retryPayments(payments []*models.Payment) {
	time.Sleep(10 * time.Second)
	for _, p := range payments {
		c.paymentChan <- p
	}
}


// Worker that aggregates transactions and sends them (scoped) to the network
// FIXME: move currentTx, currentDbTx from the global scope
func (c *Connector) TxBuilder() {
	var ctx context.Context
	var err error
	var usedUtxos []uint64
	var payments []*models.Payment

	for {
		if c.currentTx == nil {
			ctx, _ = context.WithTimeout(c.ctx, time.Duration(10 * time.Second))
			if c.currentDbTx != nil {
				c.currentDbTx.RollbackTransaction()
			}
			c.currentDbTx, err = c.storage.BeginTransaction()

			if err != nil {
				log.Error(err)
				time.Sleep(time.Second)
				continue
			}

			c.currentTx = &rawTransaction{
				msgTx: wire.NewMsgTx(wire.TxVersion),
			}

			usedUtxos = make([]uint64, 0)
			payments = make([]*models.Payment, 0)
		}

		select {
		case <-ctx.Done(): {
			if err := c.finalizeCryptoTx(payments); err != nil {
				log.Error(err)
				c.currentTx = nil
				// fixme: read from db
				go c.retryPayments(payments)
			}
		}
		case payment := <-c.paymentChan:
			payments = append(payments, payment)
			err := c.handlePayment(usedUtxos, payment)
			if err != nil {
				log.Error(err)
				c.currentTx = nil
				// fixme: read from db
				go c.retryPayments(payments)
			}
		case <-c.ctx.Done():
			return
		}
	}
}


// Sign all tx inputs after adding tx outputs
func (c *Connector) signTx() error {
	/*signature, _ := txscript.SignatureScript(
		bs.currentTx.msgTx,  		// The tx to be signed.
		bs.currentTx.txInIdx,		// The index of the txin the signature is for.
		txIn.SignatureScript,       // The other half of the script from the PubKeyHash.
		txscript.SigHashSingle,		// The signature flags that indicate what the sig covers.
		privateKey,          		// The key to generate the signature with.
		true,       		// The compress sig flag. This saves space on the blockchain.
	)*/

	for i, txIn := range c.currentTx.msgTx.TxIn {
		signature, err := txscript.SignTxOutput(
			c.netParams,
			c.currentTx.msgTx,
			i,
			txIn.SignatureScript,
			txscript.SigHashAll,
			txscript.KeyClosure(func(a btcutil.Address) (*btcec.PrivateKey, bool, error) {
				privateKeyBytes, err := c.storage.PrivateKey(a.String())
				privateKey, _ := btcec.PrivKeyFromBytes(btcec.S256(), privateKeyBytes)
				return privateKey, true, err
			}),
			nil, nil,
		)

		if err != nil {
			return err
		}

		txIn.SignatureScript = signature
	}

	return nil
}
