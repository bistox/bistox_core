package blockchain

import (
	"github.com/op/go-logging"
	"gitlab.com/bistox/bistox_core/mods/blockchain/connectors"
	"gitlab.com/bistox/bistox_core/mods/blockchain/connectors/bitcoind"
	"gitlab.com/bistox/bistox_core/mods/blockchain/connectors/ethereumd"
	"gitlab.com/bistox/bistox_core/mods/db/models"
	"sync"
)

const KeyName = "blockchain"


var log = logging.MustGetLogger("bistox")


type Mod struct {
	connector connectors.BlockchainConnector
	config *Config
}


type Config struct {
	Enabled bool
	Options bitcoind.Config
}


func GetDefaultConfig() *Config {
	return &Config{
		Enabled: false,
		Options: connectors.BlockchainConfig{
			MinConfirmations: 0,
			SyncLoopDelay: 10,
		},
	}
}


func (mod Mod) Cleanup(ready *sync.WaitGroup) {
	ready.Done()
}


func (Mod) KeyName() string {
	return KeyName
}


func (mod Mod) Enabled() bool {
	return mod.config.Enabled
}


func (mod Mod) Run(ready *sync.WaitGroup, quit chan<- bool) {
	if mod.connector == nil {
		ready.Done()
		log.Criticalf("Unsupported currency %v", mod.config.Options.Currency)
		quit <- true
		return
	}

    // FIXME: TODO: uncomment in prod!
	/*if err := mod.connector.ValidateConfiguration(); err != nil {
		log.Critical(err)
		quit <- true
	}*/

	ready.Done()
}


func (mod Mod) Connector() connectors.BlockchainConnector {
	return mod.connector
}


func New(config *Config) Mod {
	var connector connectors.BlockchainConnector
	switch config.Options.Currency {
	case models.BTC, models.BCH, models.DASH, models.LTC:
		connector = bitcoind.NewConnector(&config.Options)
	case models.ETH:
		connector = ethereumd.NewConnector(&config.Options)
	}

	return Mod{
		connector: connector,
		config: config,
	}
}
