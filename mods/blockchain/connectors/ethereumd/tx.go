package ethereumd

import (
	"fmt"
	"github.com/ethereum/go-ethereum/common"
)


type EthereumTransaction struct {
	TxId string
	TxIdRaw []uint8
	IsCoinbase bool
	From common.Address
	To common.Address
	Amount int64
	Fee int64
}


func (et EthereumTransaction) String() string {
	return fmt.Sprintf("Tx %s with amount %v {\nFrom: %v\nTo: %v}", et.TxId,  et.Amount, et.From.Hex(), et.To.Hex())
}
