package ethereumd

import (
	"context"
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"fmt"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/crypto"
	"github.com/ethereum/go-ethereum/ethclient"
	"github.com/op/go-logging"
	"gitlab.com/bistox/bistox_core/etc"
	"gitlab.com/bistox/bistox_core/mods/blockchain/connectors"
	"gitlab.com/bistox/bistox_core/mods/db/models"
	"math/big"
	"regexp"
	"strings"
)


var log = logging.MustGetLogger("bistox")

type Config = connectors.BlockchainConfig

var (
	weiInEth = big.NewInt(1e18)
	defaultWallet = "exchange"
	defaultTxGas int64 = 90000
)


type Connector struct {
	ctx context.Context
	storage connectors.BlockchainStorage
	cache connectors.BlockchainCache

	config *Config
	client *ethclient.Client
	bestBlockHeight uint32
	paymentChan chan *models.Payment

	quit chan bool
}


func (c *Connector) ValidateConfiguration() error {
	if c.config.Addr == "" {
		return etc.Error(etc.InvalidConfig, "Eth rpc address must be specified")
	}
	network, err := c.client.NetworkID(context.Background())
	if err != nil {
		return etc.InternalErrorf("Unable to get net version: %v", err)
	}

	net := convertNet(network)
	if c.config.Net != net {
		return etc.InternalErrorf("Networks are different! Desired: %s, actual: %s", c.config.Net, net)
	}
	log.Infof("Init connector working with '%v' net", net)
	return nil
}


func (c *Connector) CreateAddress() (privateKeyBytes []byte, publicKeyBytes []byte, address string, err error) {
	privateKey, err := ecdsa.GenerateKey(crypto.S256(), rand.Reader)
	if err != nil {
		return nil, nil, "", nil
	}
	privateKeyBytes = crypto.FromECDSA(privateKey)
	publicKey := ecdsa.PublicKey{
		Curve: crypto.S256(),
		X: privateKey.X,
		Y: privateKey.Y,
	}
	publicKeyBytes = elliptic.Marshal(crypto.S256(), privateKey.X, privateKey.Y)
    address = strings.ToLower(crypto.PubkeyToAddress(publicKey).Hex())

	return privateKeyBytes, publicKeyBytes, address, nil
}


func (c *Connector) InsertAddress(address *models.CryptoAddress) error {
	return c.cache.InsertAddress(address)
}


func (c *Connector) ConfirmedBalance(address string) (int64, error) {
	return 0, nil
}


func (c *Connector) PendingBalance(address string) (int64, error) {
	if data, err := c.cache.GetAddress("address"); err != nil {
		return 0, err
	} else {
		return data.Balance, nil
	}
}


func (c *Connector) PendingTransactions(accountAlias string) ([]*models.Payment, error) {
	return nil, nil
}


func (c *Connector) PaymentChannel() chan<- *models.Payment {
	return c.paymentChan
}


func (c *Connector) ValidateAddress(address string) error {
	re := regexp.MustCompile(fmt.Sprintf(`(?:0x)?[0-9a-fA-F]{%d}`, common.AddressLength * 2))

	if !common.IsHexAddress(address) || !re.MatchString(address) {
		return etc.InternalError("invalid hex address")
	}

	return nil
}


func (c *Connector) EstimateFee(amount int64) (int64, error) {
	gp, err := c.client.SuggestGasPrice(c.ctx)
	if err != nil {
		return 0, err
	}

	gasPrice := big.NewInt(gp.Int64())

	gas := big.NewInt(defaultTxGas)
	txFee := new(big.Int).Mul(gas, gasPrice)

	return weiToSzabo(txFee.Div(txFee, weiInEth)), nil
}


func (c *Connector) BestBlock() (uint32, string, error) {
	header, err := c.client.HeaderByNumber(c.ctx, nil)
	if err != nil {
		return 0, "", err
	}
	block, err := c.client.BlockByNumber(c.ctx, header.Number)
	if err != nil {
		return 0, "", err
	}
	return uint32(block.Number().Uint64()), block.Hash().Hex(), nil
}


func (c *Connector) SyncNextBlock(prevHash string) (nextHash string, payments []connectors.Tx, err error) {
	prevBlock, err := c.client.BlockByHash(c.ctx, common.HexToHash(prevHash))

	if err != nil {
		return "", nil, etc.InternalErrorf("Unable to process block: %v", err)
	}

	block, err := c.client.BlockByNumber(c.ctx, big.NewInt(0).Add(prevBlock.Number(), big.NewInt(1)))

	if err != nil {
		if err.Error() == "not found" {
			return "", nil, etc.Error(etc.Ignored, "Next blockhash is empty")
		}
		return "", nil, etc.InternalErrorf("Unable to process block: %v", err)
	}

	transactions, err := c.handleBlock(block)

	if err != nil {
		return "", nil, etc.InternalErrorf("Unable to process block: %v", err)
	}

	return block.Hash().Hex(), transactions, nil
}


func (c *Connector) handleBlock(block *types.Block) ([]connectors.Tx, error) {
	unclesLength := len(block.Uncles())
	txs := make([]connectors.Tx, len(block.Transactions()) + 1 + unclesLength)

	i := 0

	for _, tx := range block.Transactions() {
		from, err := c.client.TransactionSender(c.ctx, tx, block.Hash(), uint(i))

		if err != nil {
			return nil, err
		}

		if tx.To() == nil {
			continue
		}

		// In ETH (not contract) tx always one output and one input
		t := connectors.Tx{
			TxId: tx.Hash().Bytes(),
			Data: EthereumTransaction{
				TxIdRaw: tx.Hash().Bytes(),
				TxId: strings.ToLower(tx.Hash().Hex()),
				Amount: weiToSzabo(tx.Cost()),
				From: from,
				To: *tx.To(),
				Fee: weiToSzabo(tx.Cost()) - weiToSzabo(tx.Value()),
			},
		}

		txs[i] = t
		i++
	}

	txs[i] = connectors.Tx{
		TxId: block.TxHash().Bytes(),
		Data: EthereumTransaction{
			TxId: block.Hash().Hex(),
			TxIdRaw: block.Hash().Bytes(),
			From: common.Address{},
			To: block.Coinbase(),
			IsCoinbase: true,
			Amount: StaticBlockReward * (1 + int64(unclesLength) / 32), // + Fee!
			Fee: 0,
		},
	}

	i++

	for _, uncle := range block.Uncles() {
		txs[i] = connectors.Tx{
			TxId: uncle.TxHash.Bytes(),
			Data: EthereumTransaction{
				TxId: uncle.Hash().Hex(),
				TxIdRaw: uncle.Hash().Bytes(),
				From: common.Address{},
				To: uncle.Coinbase,
				IsCoinbase: true,
				Amount: StaticBlockReward * (block.Number().Int64() + 8 - uncle.Number.Int64()) / 8,
				Fee: 0,
			},
		}

		i++
	}

	return txs, nil
}


func (c *Connector) Currency() models.CryptoCurrency {
	return c.config.Currency
}


func (c *Connector) GetSyncedHeight() uint32 {
	return c.bestBlockHeight
}


func (c *Connector) SetSyncedHeight(height uint32) {
	c.bestBlockHeight = height
}


func (c *Connector) MinConfirmations() int {
	return c.config.MinConfirmations
}


func (c *Connector) ConfirmedHeight() uint32 {
	if c.config.MinConfirmations >= int(c.bestBlockHeight) {
		return 0
	} else {
		return c.bestBlockHeight - uint32(c.config.MinConfirmations)
	}
}


func (c *Connector) Install(ctx context.Context, storage connectors.BlockchainStorage, cache connectors.BlockchainCache) {
	c.ctx = ctx
	c.cache = cache
	c.storage = storage
}


// Force check interface implementation
var _ connectors.BlockchainConnector = (*Connector)(nil)
var _ connectors.BlockchainConnectorInner = (*Connector)(nil)


func NewConnector(config *Config) connectors.BlockchainConnector {
	client, err := ethclient.Dial(config.Addr)

	if err != nil {
		panic(err)
	}

	connector := Connector{
		config: config,
		client: client,
		quit: make(chan bool),
		paymentChan: make(chan *models.Payment),
	}

	return &connector
}
