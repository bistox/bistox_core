package models

import (
	"github.com/go-pg/pg"
	"golang.org/x/crypto/bcrypt"
)


type Profile struct {
	BaseModel
	tableName struct{} 	`sql:"profiles,alias:p"`
	Login string 		`sql:",unique,notnull"`
	Password string 	`sql:",notnull"`
	Email string 		`sql:",unique,notnull"`
	Tokens []*Token		`pg:"fk:profile_id"`
}


func (p Profile) Prepare(db *pg.DB, force bool) error {
	return nil
}


func (p Profile) Sync(db *pg.DB, force bool) error {
	if _, err := db.Model((*Profile)(nil)).Exec(CreateIndexQuery(true,"login")); err != nil {
		return err
	}
	if _, err := db.Model((*Profile)(nil)).Exec(CreateIndexQuery(true,"email")); err != nil {
		return err
	}
	return nil
}


func (p *Profile) Validate() error {
	return nil
}


func (p *Profile) HashPassword() {
	hashedPassword, _ := bcrypt.GenerateFromPassword([]byte(p.Password), 10)
	p.Password = string(hashedPassword)
}


func (p *Profile) CheckPassword(password string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(p.Password), []byte(password))
	return err == nil
}
