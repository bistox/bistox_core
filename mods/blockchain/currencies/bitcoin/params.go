package bitcoin

import (
	"github.com/btcsuite/btcd/chaincfg"
	"gitlab.com/bistox/bistox_core/etc"
)


func GetParams(netName string) (*chaincfg.Params, error) {
	switch netName {
	case "mainnet", "main":
		return &chaincfg.MainNetParams, nil
	case "simnet":
		return &chaincfg.SimNetParams, nil
	case "regtest":
		return &chaincfg.RegressionNetParams, nil
	case "testnet3", "test", "testnet":
		return &chaincfg.TestNet3Params, nil
	}

	return nil, etc.InternalErrorf("network '%s' is invalid or unsupported", netName)
}