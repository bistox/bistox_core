package bistox_core

import (
	"fmt"
)


const (
	appMajor uint = 0
	appMinor uint = 0
	appPatch uint = 1

	appPreRelease = "alpha"
)


func version() string {
	version := fmt.Sprintf("%d.%d.%d", appMajor, appMinor, appPatch)

	if appPreRelease != "" {
		version = fmt.Sprintf("%s-%s", version, appPreRelease)
	}

	return version
}
