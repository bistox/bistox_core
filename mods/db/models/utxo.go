package models

import (
	"encoding/hex"
	"fmt"
	"github.com/go-pg/pg"
	"github.com/tilinna/z85"
	"gitlab.com/bistox/bistox_core/etc"
)


/*
 Bitcoin-like balance representation
 (an unspent transaction output (utxo) that can be spent as an input in a new transaction)
*/
type Utxo struct {
	BaseModel
	ID uint64                   `sql:",pk"`
	tableName struct{} 			`sql:"utxos,alias:utxo"`
	CurrencyCode CurrencyEnum	`sql:",type:currency_code,notnull,"`
	Amount int64				`sql:",notnull"`

	AddressId uint64			`sql:",notnull"`
	Address *CryptoAddress		`pg:"fk:address_id"`

	BlockHeight uint32			`sql:",notnull,default:0"`
	TxId []byte					`sql:",notnull"`
	Vout byte					`sql:",notnull"`

	//deprecated field
	ToTxId []byte				`sql:","`
}


func (Utxo) Prepare(db *pg.DB, force bool) error {
	return nil
}


func (Utxo) Sync(db *pg.DB, force bool) error {
	_, err := db.Model((*Utxo)(nil)).Exec(CreateIndexQuery(true,"tx_id", "vout"))
	return err
}


func (u *Utxo) String() string {
	return fmt.Sprintf(
		"%s[sum=%d, addr_id=%d, txid=%s, vout=%d]",
		u.CurrencyCode, u.Amount, u.AddressId,
		hex.EncodeToString(*etc.ReverseBytes(&u.TxId)),
		u.Vout,
	)
}


func (u *Utxo) CompactTxOutString() string {
	z85encoded := make([]uint8, 2 * len(u.TxId))
	count, _ := z85.Encode(z85encoded, u.TxId)
	return string(z85encoded[:count]) + hex.EncodeToString([]byte{u.Vout})
}


func DecodeCompactTxOut(tx string) ([]uint8, uint8) {
	txId, voutStr := tx[:(len(tx)-2)], tx[len(tx)-2:]
	vout, _ := hex.DecodeString(voutStr)
	z85decoded := make([]uint8, len(txId))
	count, _  := z85.Decode(z85decoded, []byte(txId))
	return z85decoded[:count], vout[0]
}
