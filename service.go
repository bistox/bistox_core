package bistox_core

import (
	"context"
	"github.com/go-pg/pg"
	"github.com/go-redis/redis"
	"github.com/op/go-logging"
	"gitlab.com/bistox/bistox_core/etc"
	"gitlab.com/bistox/bistox_core/mods/blockchain"
	"gitlab.com/bistox/bistox_core/mods/blockchain/connectors"
	"gitlab.com/bistox/bistox_core/mods/cache"
	"gitlab.com/bistox/bistox_core/mods/db"
	"gitlab.com/bistox/bistox_core/mods/server"
	"os"
	"os/signal"
	"runtime"
	"sync"
	"syscall"
)

var log = logging.MustGetLogger("bistox")


type Service struct {
	Config Config
	onceConfig sync.Once
	onceMods sync.Once
	mods map[string] Mod
	context context.Context
	quit chan bool
	wg sync.WaitGroup
	onready []func(context.Context)
}


func (s *Service) initMods() {
	s.onceMods.Do(func() {
		mods := []Mod{
			db.New(s.Config.Db),
			server.New(s.Config.Server),
			blockchain.New(s.Config.Blockchain),
			cache.New(s.Config.Cache),
		}

		for _, m := range mods {
			if m.Enabled() {
				s.mods[m.KeyName()] = m
			}
		}
	})
}


// Read config file and cli args
func (s *Service) initConfig() error {
	var err error = nil
	s.onceConfig.Do(func() {
		s.Config = getDefaultConfig()
		err = s.Config.loadConfig()
	})
	return err
}


func (s *Service) Init() error {
	err := s.initConfig()
	return err
}


// TODO: Need to add mod dependencies
func (s *Service) cleanup(withError bool) {
	log.Warning("Stopping...")
	s.wg.Add(len(s.mods))
	for _, m := range s.mods {
		m.Cleanup(&s.wg)
	}
	s.wg.Wait()
	log.Info("Service has stopped")

	code := 0
	if withError {
		code = 1
	}
	os.Exit(code)
}


func (s *Service) runMods() {
	for _, m := range s.mods {
		if m.Enabled() {
			log.Infof(`Inner module "%s" has been enabled`, m.KeyName())
			s.wg.Add(1)
			go m.Run(&s.wg, s.quit)
		}
	}
	s.wg.Wait()
}


func (s *Service) Run() error {
	if err := s.initConfig(); err != nil {
		return err
	}

	s.initMods()
	s.runMods()
	log.Info("Service is ready!")

	ch := make(chan os.Signal, 1)
	signal.Notify(ch, syscall.SIGTERM, syscall.SIGINT, syscall.SIGQUIT)

	ctx, cancel := context.WithCancel(s.context)
	for _, f := range s.onready {
		go f(ctx)
	}

	withError := false

	select {
		// wait on kill signal
		case <-ch: {}
		// wait on quit signal
		case <-s.quit:
			withError = true
	}

	cancel()
	s.cleanup(withError)
	close(s.quit)

	return nil
}


func (s *Service) Kill() {
	s.quit <- true
}


func (s *Service) OnReady(f func(context.Context)) {
	s.onready = append(s.onready, f)
}


func (s Service) String() string {
	return "Bistox core " + version()
}


func NewService(verbosity logging.Level) Service {
	etc.SetupLogger(verbosity)
	// Use all processor cores
	runtime.GOMAXPROCS(runtime.NumCPU())

	service := Service{
		Config: getDefaultConfig(),
		context: context.Background(),
		onready: []func(context.Context){},
		mods: map[string]Mod{},
		quit: make(chan bool, 0xff),
		wg: sync.WaitGroup{},
	}

	log.Info(service)

	return service
}


func (s *Service) Context() context.Context {
	return s.context
}

// Explicit instances
func (s *Service) DB() *pg.DB {
	if s.Config.Db.Enabled {
		return s.mods[db.KeyName].(db.Mod).Db
	}
	return nil
}

func (s *Service) BlockchainConnector() connectors.BlockchainConnector {
	if s.Config.Blockchain.Enabled {
		return s.mods[blockchain.KeyName].(blockchain.Mod).Connector()
	}
	return nil
}

func (s *Service) Cache() *redis.Client {
	if s.Config.Cache.Enabled {
		return s.mods[cache.KeyName].(cache.Mod).Cache
	}
	return nil
}

func (s *Service) Server() *server.Mod {
	if s.Config.Server.Enabled {
		m := s.mods[server.KeyName].(server.Mod)
		return &m
	}
	return nil
}
