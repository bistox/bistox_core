package db

import (
	"github.com/go-pg/pg"
	"github.com/op/go-logging"
	s "sync"
	"time"
)

var log = logging.MustGetLogger("bistox")

const HealthCheckInterval = 10 * time.Second
const KeyName = "db"


type Mod struct {
	Db *pg.DB
	config *Config
}


type Config struct {
	Enabled bool
	Options *pg.Options
}


func GetDefaultConfig() *Config {
	return &Config{
		Enabled: false,
		Options: &pg.Options{
			Addr: "localhost:5432",
			Database: "bistox",
			User: "bistox",
			Password: "bistox",
			PoolSize: 5,
		},
	}
}


func (mod Mod) Cleanup(ready *s.WaitGroup) {
	mod.Db.Close()
	log.Info("Disconnected from PG")
	ready.Done()
}


func (Mod) KeyName() string {
	return KeyName
}


func (mod Mod) Enabled() bool {
	return mod.config.Enabled
}


func (mod Mod) Run(ready *s.WaitGroup, quit chan<- bool) {
	mod.Db.AddQueryHook(eventHook{allQueries: true})

	if healthCheck(mod.Db) == nil {
		log.Info("Connected to PG!")
	}
	// Automatic migration and synchronization of tables, creating indexes
	if err := sync(mod.Db, false); err != nil {
		log.Criticalf("Postgres error: %v", err)
		quit <- true
	}

	ready.Done()

	for {
		if err := healthCheck(mod.Db); err != nil {
			log.Errorf("Postgres healthcheck error: %v", err)
			quit <- true
		}
		time.Sleep(HealthCheckInterval)
	}
}


func healthCheck(db *pg.DB) error {
	var testRes bool
	if _, err := db.Query(&testRes, `select true;`); err != nil || !testRes {
		log.Criticalf("Postgres error: %v", err)
		return err
	}
	return nil
}


func New(config *Config) Mod {
	db := pg.Connect(config.Options)
	return Mod{db, config}
}
