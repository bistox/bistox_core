package connectors

import (
	"context"
	"github.com/go-pg/pg"
	"github.com/op/go-logging"
	"gitlab.com/bistox/bistox_core/etc"
	"gitlab.com/bistox/bistox_core/mods/db/models"
	"strconv"
	"strings"
	"time"
)

var log = logging.MustGetLogger("bistox")


// Asynchronously receives only succeeded ACID transactions
func txEventChannel(newTx <-chan *Event) {
	for {
		tx := <-newTx
		if tx == nil {
			continue
		}
		payload := strings.Split(tx.Payload, " ")
		switch tx.Channel {
		// Tx events
		case "new_tx":
			log.Debugf("New tx event received: %v", payload)
		case "del_tx":
			log.Debugf("Del tx event received: %v", payload)
		case "new_block":
			log.Infof("New block event received: %s", payload)

		// Wallet events
		case "new_address":
			log.Infof("New address event received: %s", payload)
		}
	}
}


// Internal connector loop
func SyncLoop(connector BlockchainConnector, ctx context.Context, storage BlockchainStorage, cache BlockchainCache) error {
	inner := connector.(BlockchainConnectorInner)
	inner.Install(ctx, storage, cache)
	log.Infof("Started %s blockchain syncing ...", connector.Currency())

	bestBlockHeight, bestBlockHash, err := connector.BestBlock()
	if err != nil {
		return err
	}
	// Get last synced block
	block, err := storage.GetBlock(false)
	if err != nil {
		return err
	}
	for block.Hash == "" {
		block.Height = bestBlockHeight
		block.Hash = bestBlockHash
	}
	inner.SetSyncedHeight(block.Height)
	log.Debugf("Best block height: %v", bestBlockHeight)

	// Cache
	if err := warmUpWallets(storage, cache); err != nil {
		return err
	}
	if err := inner.WarmUpCache(true); err != nil {
		return err
	}
	//

	go inner.TxBuilder()

	// Send waiting payments to the tx builder
	preparingPayments, err := storage.PreparingPayments()
	if err != nil {
		return err
	}
	for _, pp := range preparingPayments {
		connector.PaymentChannel() <- &pp
	}

	txEventListener := storage.Listen("new_tx", "del_tx", "new_block")
	go txEventChannel(txEventListener.Channel())
	defer txEventListener.Close()

	for {
		select {
		case <-ctx.Done():
			return nil
		default: {}
		}

		nextHash, transactions, err := inner.SyncNextBlock(block.Hash)

		if err != nil || nextHash == "" {
			if err.(etc.BistoxError).BistoxErrorType != etc.Ignored {
				log.Error(err)
			}
			time.Sleep(time.Second)
			continue
		}

		block.Hash = nextHash
		block.Height++

		// Data for the memory cache updating
		var cacheBalances map[string]int64

		retryTransaction(storage, func(tx BlockchainStorage) error {
			cacheBalances = map[string]int64{}

			// Acquire a lock on block
			if b, err := tx.GetBlock(true); err != nil {
				return err
			} else {
				if b.Height >= block.Height {
					return nil
				}
			}

			var newPayments []*models.Payment

			// 1. Sync crypto transactions into db
			for _, t := range transactions {
				exist, err := storage.CheckPaymentsByTx(t.TxId, block.Height)

				if err != nil {
					return err
				}

				if exist {
					continue
				}

				if err := inner.SyncTransaction(tx, t, &newPayments); err != nil {
					return err
				}
			}

			if err := tx.UpdateBlock(block); err != nil {
				return err
			}

			for _, p := range newPayments {
				// Only when there is a manual withdrawal of funds from the exchange
				if p.Direction == models.DirectionOutgoing {
					if err := tx.UpdateWalletBalance(p.WalletId, p.Amount); err != nil { return err }
					//if err := tx.UpdateAddressBalance(p.AddressId, p.Amount); err != nil { return err }
				} // !! TODO: remove UTXO from db and cache

				p.BlockHeight = block.Height
				p.CurrencyCode = connector.Currency()
				if err := tx.Insert(p); err != nil {
					return err
				}
				for _, cp := range p.CryptoPayments {
					cp.PaymentId = p.ID
					if err := tx.Insert(cp); err != nil {
						return err
					}
					if err := tx.UpdateAddressBalance(cp.AddressId, cp.Amount, false); err != nil {
						return err
					}
				}
			}

			h := block.Height - uint32(connector.MinConfirmations())
			if confirmedPayments, err := tx.PaymentsAtHeight(h); err != nil {
				return err
			} else {
				for _, p := range confirmedPayments {
					p.Status = models.StatusCompleted

					if p.Direction == models.DirectionIncoming {
						if err := tx.UpdateWalletBalance(p.WalletId, p.Amount); err != nil { return err }
					}

					for _, cp := range p.CryptoPayments {
						cacheBalances[cp.Address.Address] += cp.Amount
						if err := tx.UpdateAddressBalance(cp.AddressId, cp.Amount, true); err != nil {
							return err
						}
					}

					if err := tx.SetPaymentStatus(&p); err != nil { return err }
				}
			}

			if err := tx.Notify("new_block", strconv.Itoa(int(block.Height))); err != nil {
				return err
			}

			return nil
		})

		for address, amount := range cacheBalances {
			cache.UpdateAddressBalance(address, amount)
		}

		if block.Height > inner.GetSyncedHeight() {
			inner.SetSyncedHeight(block.Height)
			log.Infof("%v synced...", block)
			inner.WarmUpCache(false)
		}
	}
	return nil
}


func retryTransaction(storage BlockchainStorage, f func(tx BlockchainStorage) error) {
	for err := storage.RunInTransaction(f); err != nil && err != pg.ErrNoRows; err = storage.RunInTransaction(f) {
		log.Error("!", err)
		time.Sleep(time.Second)
	}
}


func warmUpWallets(storage BlockchainStorage, cache BlockchainCache) error {
	const limit = 10000
	offset := 0
	wallets := make([]*models.Wallet, 1)

	for len(wallets) > 0 {
		if err := storage.GetWallets(&wallets, limit, offset); err != nil {
			return err
		}
		offset += limit

		for _, wallet := range wallets {
			if err := cache.InsertWallet(wallet); err != nil {
				return err
			}
			for _, addr := range wallet.Addresses {
				addr.Wallet = wallet

				log.Debug(addr.Address, wallet.Type)

				if err := cache.InsertAddress(addr); err != nil {
					return err
				}
			}
		}
	}

	return nil
}
