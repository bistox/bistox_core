package models

import (
	"github.com/go-pg/pg"
)


type TradeHistory struct {
	BaseModel
	ID uint64							`sql:",pk"`
	tableName 			struct{}        `sql:"trade_history,alias:th"`
	Price              	int64			`sql:",notnull"`
	Volume              int64			`sql:",notnull"`
	BaseCurrencyCode   	CurrencyEnum	`sql:",type:currency_code,notnull,"`
	QuotedCurrencyCode 	CurrencyEnum	`sql:",type:currency_code,notnull,"`
}

func (o *TradeHistory) Prepare(db *pg.DB, force bool) error {
	return nil
}


func (o *TradeHistory) Sync(db *pg.DB, force bool) error {
	_, err := db.Model((*TradeHistory)(nil)).Exec(CreateIndexQuery(
		false,"created_at", "base_currency_code", "quoted_currency_code"))
	_, err = db.Model((*TradeHistory)(nil)).Exec(CreateIndexQuery(
		false,"base_currency_code", "quoted_currency_code", "created_at"))
	return err
}


/*
func (o *TradeHistory) ToProto() *pb.Order {
	createdAt, _ := ptypes.TimestampProto(o.CreatedAt)
	var deletedAt *timestamp.Timestamp
	if o.DeletedAt != nil {
		deletedAt, _ = ptypes.TimestampProto(*o.DeletedAt)
	} else {
		deletedAt, _ = ptypes.TimestampProto(time.Time{})
	}

	return &pb.Order{
		OrderId: o.ID,
		OrderType: o.OrderType.ToProto(),
		OperationType: o.OperationType.ToProto(),
		BaseCurrency: o.BaseCurrencyCode.ToProto(),
		QuotedCurrency: o.QuotedCurrencyCode.ToProto(),
		Amount: float64(o.Amount) / float64(Divisor[o.BaseCurrencyCode]),
		Price: float64(o.Price) / float64(Divisor[o.QuotedCurrencyCode]),
		FilledAmount: float64(o.FilledAmount) / float64(Divisor[o.BaseCurrencyCode]),
		FilledPrice: float64(o.FilledPrice) / float64(Divisor[o.QuotedCurrencyCode]),
		Canceled: o.Canceled,
		Failed: o.Failed,
		CreatedAt: createdAt,
		DeletedAt: deletedAt,
	}
}

func (orderType OrderType) ToProto() pb.OrderType {
	return pb.OrderType(pb.OrderType_value[string(orderType)])
}
*/