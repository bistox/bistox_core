package ethereumd

import (
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"github.com/ethereum/go-ethereum/crypto"
	"github.com/ethereum/go-ethereum/params"
	"math/big"
	"strings"
)


// Or 1 MicroEther
const Szabo = 1
const Ether = 1000000 * Szabo
const StaticBlockReward = 5 * Ether

var WeiInSzabo = big.NewInt(1000000000000)


func convertNet(id *big.Int) string {
	net := "simnet"

	switch id {
	case params.RinkebyChainConfig.ChainID:
		net = "testnet"
	case params.MainnetChainConfig.ChainID:
		net = "mainnet"
	}

	return net
}


func weiToSzabo(amount *big.Int) int64 {
	return big.NewInt(0).Div(amount, WeiInSzabo).Int64()
}


func szaboToWei(amount int64) *big.Int {
	return big.NewInt(0).Mul(big.NewInt(amount), WeiInSzabo)
}


func NewAddress() (privateKeyBytes []byte, publicKeyBytes []byte, address string, err error) {
	privateKey, err := ecdsa.GenerateKey(crypto.S256(), rand.Reader)
	if err != nil {
		return nil, nil, "", nil
	}
	privateKeyBytes = crypto.FromECDSA(privateKey)
	publicKey := ecdsa.PublicKey{
		Curve: crypto.S256(),
		X: privateKey.X,
		Y: privateKey.Y,
	}
	publicKeyBytes = elliptic.Marshal(crypto.S256(), privateKey.X, privateKey.Y)
	address = strings.ToLower(crypto.PubkeyToAddress(publicKey).Hex())

	return privateKeyBytes, publicKeyBytes, address, nil
}
