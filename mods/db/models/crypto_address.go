package models

import "github.com/go-pg/pg"


type CryptoAddress struct {
	BaseModel
	ID uint64					`sql:",pk"`
	Address string 				`sql:",notnull,type:varchar(42)"`
	tableName struct{} 			`sql:"crypto_addresses,alias:ca"`
	// Blockchain balance!
	Balance int64				`sql:"default:0,notnull"`
	Wallet *Wallet				`pg:"fk:wallet_id"`
	WalletId string				`sql:",type:uuid"`
	CurrencyCode CurrencyEnum	`sql:",type:currency_code,notnull,"`
	PrivateKey []byte  			`sql:",notnull"`
	Nonce uint64				`sql:",notnull,default:0"`
}


func (ca CryptoAddress) Prepare(db *pg.DB, force bool) error {
	return nil
}


func (ca CryptoAddress) Sync(db *pg.DB, force bool) error {
	_, err := db.Model((*CryptoAddress)(nil)).Exec(CreateIndexQuery(false,"wallet_id"))
	db.Model((*CryptoAddress)(nil)).Exec(CreateIndexQuery(true,"address"))
	return err
}


func (ca *CryptoAddress) String() string {
	return ca.Address
}
