package models

import (
	"github.com/go-pg/pg"
)


// Address2Address
type CryptoPayment struct {
	BaseModel
	ID uint64                   `sql:",pk"`
	tableName struct{} 			`sql:"crypto_payments,alias:cpm"`

	AddressId uint64			`sql:",notnull"`
	Address *CryptoAddress		`pg:"fk:address_id"`

	PaymentId uint64			`sql:",notnull"`
	Payment *Payment		    `pg:"fk:payment_id"`

	Amount int64				`sql:",notnull"`
}


func (cp CryptoPayment) Prepare(db *pg.DB, force bool) error {
	return nil
}


func (cp CryptoPayment) Sync(db *pg.DB, force bool) error {
	return nil
}
