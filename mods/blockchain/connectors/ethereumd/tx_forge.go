package ethereumd

import (
	"context"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/crypto"
	"gitlab.com/bistox/bistox_core/etc"
	"gitlab.com/bistox/bistox_core/mods/blockchain/connectors"
	"gitlab.com/bistox/bistox_core/mods/blockchain/currencies/ethereum"
	"gitlab.com/bistox/bistox_core/mods/db/models"
	"math/big"
	"strings"
	"time"
)


func (c *Connector) TxBuilder() {
	go c.paymentsRedirector()

	var ctx context.Context
	for {
		ctx, _ = context.WithTimeout(c.ctx, time.Duration(10 * time.Second))

		select {
		case <-ctx.Done(): {}
		case payment := <-c.paymentChan:
			err := c.handlePayment(payment)
			if err != nil {
				log.Error(err)
				// fixme: read from db
				go func() {
					time.Sleep(time.Second)
					c.paymentChan <- payment
				}()
			}
		case <-c.ctx.Done():
			return
		}
	}
}


func (c *Connector) paymentsRedirector() {
	var duration time.Duration

	for {
		select {
		case <-c.ctx.Done():
			return
		case <-time.After(duration):
		}

		address, balance, err := c.cache.PositiveDepositAddress()

		if err != nil {
			duration = time.Second
			continue
		}

		if address == "" || balance == 0 {
			duration = 10 * time.Second
			continue
		}

		addr, err := c.cache.GetAddress(address)

		if err != nil {
			duration = time.Second
			continue
		}

		exAddress, _, err := c.cache.ExchangeWorstAddress()

		if err != nil {
			duration = time.Second
			continue
		}

		if exAddress == "" {
			log.Error("Exchange wallet has no addresses!")
			duration = 10 * time.Second
			continue
		}

		if err := c.storage.RunInTransaction(func(tx connectors.BlockchainStorage) error {
			p := &models.Payment{
				WalletId: addr.WalletId,
				Amount: 0,
				Direction: models.DirectionInternal,
				Status: models.StatusPreparing,
				CurrencyCode: c.Currency(),
				ExtAddress: exAddress,
			}

			if err := tx.Insert(p); err != nil {
				return err
			}

			if err := tx.Insert(&models.CryptoPayment{
				AddressId: addr.ID,
				Amount: -balance,
				PaymentId: p.ID,
			}); err != nil {
				return err
			}

			if err := tx.UpdateAddressBalance(addr.ID, -balance, false); err != nil {
				return err
			}

			if err := c.cache.UpdateAddressBalance(address, -balance); err != nil {
				return err
			}

			return nil
		}); err != nil {
			duration = time.Second
			continue
		}

		duration = 0
	}
}


func (c *Connector) handlePayment(payment *models.Payment) error {
	gasPrice, err := c.client.SuggestGasPrice(c.ctx)

	if err != nil {
		return err
	}

	fee := weiToSzabo(big.NewInt(1).Mul(gasPrice, big.NewInt(defaultTxGas)))

	if err != nil {
		return err
	}

	var fromAddress, toAddress string
	var fromBalance int64

	if payment.Direction == models.DirectionInternal {
		cp := payment.CryptoPayments[0]
		fromAddress = cp.Address.Address
		fromBalance = -cp.Amount
		toAddress = payment.ExtAddress
	} else {
		fromAddress, fromBalance, err = c.cache.ExchangeBestAddress()

		if err != nil {
			return err
		}
	}

	fromAddrData, err := c.cache.GetAddress(fromAddress)
	if err != nil {
		return err
	}

	toAddrData, err := c.cache.GetAddress(toAddress)
	if err != nil {
		return err
	}

	var stx *types.Transaction

	// Already prepared payment. Resend blockchain transaction & return
	if payment.Status == models.StatusWaiting {
		cryptoTx, err := c.storage.GetCryptoTx(payment.TxId)

		if err != nil {
			return err
		}

		stx = &types.Transaction{}
		if err := stx.UnmarshalJSON(cryptoTx.Tx); err != nil {
			return err
		}

		err = c.client.SendTransaction(c.ctx, stx)

		if strings.HasPrefix(err.Error(), "known transaction") {
			return nil
		}
		
		return err
	}

	if fromBalance < (-payment.Amount) {
		log.Warning("Exchange's treasury is empty!")
		return etc.Error(etc.Ignored, "EmptyExchangeWallet")
	}

	if payment.Direction == models.DirectionOutgoing && 2 * fee >= (-payment.Amount) {
		log.Warning("Low outgoing amount. Less than gate+redirection fee")
		return etc.Error(etc.InsufficientBalance, "LowBalance")
	}

	cryptoPayment := &models.CryptoPayment{
		PaymentId: payment.ID,
	}
	switch payment.Direction {
	case models.DirectionInternal:
		cryptoPayment.Amount = -payment.CryptoPayments[0].Amount - fee
		cryptoPayment.AddressId = toAddrData.ID
	case models.DirectionOutgoing:
		cryptoPayment.Amount = payment.Amount + fee
		cryptoPayment.AddressId = fromAddrData.ID
	}

	err = c.storage.RunInTransaction(func(tx connectors.BlockchainStorage) error {
		var szaboAmount *big.Int
		if payment.Direction == models.DirectionOutgoing {
			if err := tx.UpdateAddressBalance(fromAddrData.ID, cryptoPayment.Amount + fee, false); err != nil {
				return err
			}
			szaboAmount = szaboToWei(-cryptoPayment.Amount - 2 * fee)
			payment.Fee += 2 * fee

			if err = c.cache.UpdateAddressBalance(fromAddress, cryptoPayment.Amount); err != nil {
				return err
			}
		}
		if payment.Direction == models.DirectionInternal {
			if err := tx.UpdateAddressBalance(toAddrData.ID, cryptoPayment.Amount, false); err != nil {
				return err
			}
			szaboAmount = szaboToWei(cryptoPayment.Amount)
			payment.Fee += fee
			if err = c.cache.UpdateAddressBalance(toAddress, cryptoPayment.Amount); err != nil {
				return err
			}
		}

		nonce, err := tx.IncrementAddressNonce(fromAddrData.ID)

		if err != nil {
			return err
		}

		privateKeyBytes, err := tx.PrivateKey(fromAddress)

		if err != nil {
			return err
		}

		privateKey := crypto.ToECDSAUnsafe(privateKeyBytes)

		// Gateway fee + redirection fee
		transaction := types.NewTransaction(
			nonce,
			common.HexToAddress(payment.ExtAddress),
			szaboAmount,
			uint64(defaultTxGas),
			gasPrice,
		nil)

		stx, err = types.SignTx(transaction, c.getSigner(), privateKey)

		if err != nil {
			return err
		}

		stxData, err := stx.MarshalJSON()

		if err != nil {
			return err
		}

		if err := tx.Insert(&models.CryptoTransaction{
			TxId: stx.Hash().Bytes(),
			Tx: stxData,
		}); err != nil {
			return err
		}

		payment.TxId = stx.Hash().Bytes()

		if err := tx.PreparePayment(payment); err != nil {
			return err
		}

		// hold redirection fee
		if err := tx.Insert(cryptoPayment); err != nil {
			return err
		}

		return nil
	})

	if err != nil {
		return err
	}

	if err := c.client.SendTransaction(c.ctx, stx); err != nil {
		return err
	}

	return err
}


func (c *Connector) getSigner() types.Signer {
	network, _ := ethereum.GetParams(c.config.Net)
	return types.NewEIP155Signer(network.ChainID)
}