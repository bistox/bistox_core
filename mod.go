package bistox_core

import (
	"sync"
)


type Mod interface {
	Cleanup(ready *sync.WaitGroup)
	KeyName() string
	Run(ready *sync.WaitGroup, quit chan<- bool)
	Enabled() bool
}


