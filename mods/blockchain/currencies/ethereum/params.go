package ethereum

import (
	"github.com/ethereum/go-ethereum/params"
	"gitlab.com/bistox/bistox_core/etc"
)


func GetParams(netName string) (*params.ChainConfig, error) {
	switch netName {
	case "mainnet", "main":
		return params.MainnetChainConfig, nil
	case "simnet":
		return params.TestnetChainConfig, nil
	case "rinkeby":
		return params.RinkebyChainConfig, nil
	case "testnet3", "test", "testnet":
		return params.TestChainConfig, nil
	}

	return nil, etc.InternalErrorf("network '%s' is invalid or unsupported", netName)
}
