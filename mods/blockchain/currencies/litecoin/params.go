package litecoin

import (
	"github.com/btcsuite/btcd/chaincfg"
	bwire "github.com/btcsuite/btcd/wire"
	lwire "github.com/ltcsuite/ltcd/wire"
	"gitlab.com/bistox/bistox_core/etc"
)


var (
	chainIDPrefix bwire.BitcoinNet = 3
	legacyChainIDPrefix bwire.BitcoinNet = 4
)


var (
	Mainnet = bwire.MainNet + chainIDPrefix
	MainnetLegacy = bwire.MainNet + legacyChainIDPrefix
	TestNet = bwire.TestNet + chainIDPrefix
	TestNet4 = bwire.BitcoinNet(lwire.TestNet4) + chainIDPrefix
)


var MainNetParams = chaincfg.Params{
	Net:  Mainnet,
	Name: "mainnet",

	// Human-readable part for Bech32 encoded segwit addresses, as defined in
	// BIP 173.
	Bech32HRPSegwit: "ltc", // always ltc for main net

	PubKeyHashAddrID:        0x30, // starts with L
	ScriptHashAddrID:        0x32, // starts with M
	PrivateKeyID:            0xB0, // starts with 6 (uncompressed) or T (compressed)
	WitnessPubKeyHashAddrID: 0x06, // starts with p2
	WitnessScriptHashAddrID: 0x0A, // starts with 7Xh

	// BIP32 hierarchical deterministic extended key magics
	HDPublicKeyID:  [4]byte{0x04, 0x88, 0xb2, 0x1e}, // starts with xpub
	HDPrivateKeyID: [4]byte{0x04, 0x88, 0xad, 0xe4}, // starts with xprv
}


var MainNetParamsLegacy = chaincfg.Params{
	Net:  MainnetLegacy,
	Name: "mainnet-legacy",

	// Human-readable part for Bech32 encoded segwit addresses, as defined in
	// BIP 173.
	Bech32HRPSegwit: "ltc", // always ltc for main net

	PubKeyHashAddrID:        0x30, // starts with L
	ScriptHashAddrID:        0x5,  // starts with 3
	PrivateKeyID:            0xB0, // starts with 6 (uncompressed) or T (compressed)
	WitnessPubKeyHashAddrID: 0x06, // starts with p2
	WitnessScriptHashAddrID: 0x0A, // starts with 7Xh

	// BIP32 hierarchical deterministic extended key magics
	HDPublicKeyID:  [4]byte{0x04, 0x88, 0xb2, 0x1e}, // starts with xpub
	HDPrivateKeyID: [4]byte{0x04, 0x88, 0xad, 0xe4}, // starts with xprv
}


var TestNet4Params = chaincfg.Params{
	Net:  TestNet4,
	Name: "testnet4",

	// Human-readable part for Bech32 encoded segwit addresses, as defined in
	// BIP 173.
	Bech32HRPSegwit: "tltc", // always tb for test net

	// Address encoding magics
	PubKeyHashAddrID:        0x6f, // starts with m or n
	ScriptHashAddrID:        0xc4, // starts with 2
	WitnessPubKeyHashAddrID: 0x52, // starts with QW
	WitnessScriptHashAddrID: 0x31, // starts with T7n
	PrivateKeyID:            0xef, // starts with 9 (uncompressed) or c (compressed)

	// BIP32 hierarchical deterministic extended key magics
	HDPublicKeyID:  [4]byte{0x04, 0x35, 0x87, 0xcf}, // starts with tpub
	HDPrivateKeyID: [4]byte{0x04, 0x35, 0x83, 0x94}, // starts with tprv
}


var RegressionNetParams = chaincfg.Params{
	Net:  TestNet,
	Name: "regtest",

	// Human-readable part for Bech32 encoded segwit addresses, as defined in
	// BIP 173.
	Bech32HRPSegwit: "tltc", // always tltc for test net

	// Address encoding magics
	PubKeyHashAddrID: 0x6f, // starts with m or n
	ScriptHashAddrID: 0xc4, // starts with 2
	PrivateKeyID:     0xef, // starts with 9 (uncompressed) or c (compressed)

	// BIP32 hierarchical deterministic extended key magics
	HDPublicKeyID:  [4]byte{0x04, 0x35, 0x87, 0xcf}, // starts with tpub
	HDPrivateKeyID: [4]byte{0x04, 0x35, 0x83, 0x94}, // starts with tprv
}


func mustRegister(params *chaincfg.Params) {
	if err := chaincfg.Register(params); err != nil &&
		err != chaincfg.ErrDuplicateNet {
		panic("failed to register network: " + err.Error())
	}
}


func init() {
	mustRegister(&MainNetParams)
	mustRegister(&MainNetParamsLegacy)
	mustRegister(&TestNet4Params)
	mustRegister(&RegressionNetParams)
}


func GetParams(netName string) (*chaincfg.Params, error) {
	switch netName {
	case "mainnet", "main":
		return &MainNetParams, nil
	case "mainnet-legacy":
		return &MainNetParamsLegacy, nil
	case "regtest", "simnet":
		return &RegressionNetParams, nil
	case "testnet4", "test", "testnet":
		return &TestNet4Params, nil
	}

	return nil, etc.Errorf("network '%s' is invalid or unsupported", netName)
}