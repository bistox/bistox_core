package models

import (
	"github.com/go-pg/pg"
	"gitlab.com/bistox/bistox_proto"
	protoApi "gitlab.com/bistox/bistox_proto/api"
)


type WalletType string
var (
	ExchangeWallet WalletType = "Exchange"
	DepositWallet WalletType = "Deposit"
)


type Wallet struct {
	BaseModel
	tableName struct{} 			`sql:"wallets,alias:w"`
	Name string        			`sql:",notnull,type:varchar(20)"`
	Balance int64				`sql:"default:0,notnull"`
	Profile *Profile			`pg:"fk:profile_id"`
	ProfileId string			`sql:",notnull,type:uuid"`
	CurrencyCode CurrencyEnum	`sql:",type:currency_code,notnull,"`
	Type WalletType				`sql:",type:wallet_type,notnull,default:'Deposit'"`
	PrivateKey []byte  			`sql:",notnull"`  // TODO(?): Master private key for the key inheritance

	Addresses []*CryptoAddress
	Payments []*Payment
}


func (w Wallet) Prepare(db *pg.DB, force bool) error {
	if err := CreateEnum(
		db, force, "wallet_type",
		string(ExchangeWallet), string(DepositWallet),
	); err != nil {return  err}
	return nil
}


func (w Wallet) Sync(db *pg.DB, force bool) error {
	_, err := db.Model((*Wallet)(nil)).Exec(CreateIndexQuery(true,"profile_id", "name"))
	return err
}


func (w *Wallet) ToProto() *protoApi.Wallet {
	addrLen := len(w.Addresses)
	addresses := make([]*protoApi.CryptoAddress, addrLen)

	for i := 0; i < addrLen; i++ {
		addresses[i] = &protoApi.CryptoAddress{
			Address: w.Addresses[i].Address,
		}
	}

	var pendingBalance int64

	for _, pendingPayment := range w.Payments {
		pendingBalance += pendingPayment.Amount
	}

	for i := 0; i < addrLen; i++ {
		addresses[i] = &protoApi.CryptoAddress{
			Address: w.Addresses[i].Address,
		}
	}

	return &protoApi.Wallet{
		Currency: proto.Currency(proto.Currency_value[string(w.CurrencyCode)]),
		WalletId: w.ID,
		Balance: float64(w.Balance) / float64(Divisor[w.CurrencyCode]),
		Name: w.Name,
		Addresses: addresses,
		PendingBalance: float64(pendingBalance) / float64(Divisor[w.CurrencyCode]),
	}
}
