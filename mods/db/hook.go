package db

import (
	"fmt"
	"github.com/go-pg/pg"
	"strings"
	"time"
)


type eventHook struct {
	beforeQueryMethod func(*pg.QueryEvent)
	afterQueryMethod  func(*pg.QueryEvent)
	allQueries        bool
}


func (e eventHook) BeforeQuery(event *pg.QueryEvent) {
	event.Data["start_time"] = time.Now()
}


func (e eventHook) AfterQuery(event *pg.QueryEvent) {
	query, err := event.FormattedQuery()

	if err != nil {
		log.Fatalf("Error formatting query: %s", err.Error())
	}

	if strings.Contains(query, "gopg:ping") || strings.Contains(query, "select true;") {
		return
	}

	msg := fmt.Sprintf("%s %s", time.Since(event.Data["start_time"].(time.Time)), query)

	if time.Since(event.Data["start_time"].(time.Time)) >= time.Second {
		log.Warningf("Query took too long: %s", msg)
	} else if e.allQueries {
		log.Debug(msg)
	}
}