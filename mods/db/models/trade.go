package models

import "github.com/go-pg/pg"

type Trade struct {
	BaseModel
	ID uint64			`sql:",pk"`
	tableName struct{}	`sql:"trades,alias:td"`
	Maker *Order		`pg:"fk:maker_id"`
	MakerId uint64		`sql:",notnull"`
	Taker *Order		`pg:"fk:taker_id"`
	TakerId uint64		`sql:",notnull"`
	Amount int64		`sql:"default:0,notnull"`
	Price int64			`sql:"default:0,notnull"`
}

func (td Trade) Prepare(db *pg.DB, force bool) error {
	return nil
}

func (td Trade) Sync(db *pg.DB, force bool) error {
	return nil
}

func (td Trade) TotalCost() int64 {
	return td.Amount * td.Price / Divisor[td.Maker.BaseCurrencyCode]
}

