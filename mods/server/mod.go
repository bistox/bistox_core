package server

import (
	"context"
	"github.com/dgrijalva/jwt-go"
	"github.com/grpc-ecosystem/go-grpc-middleware"
	"github.com/grpc-ecosystem/go-grpc-middleware/util/metautils"
	"github.com/grpc-ecosystem/grpc-gateway/runtime"
	"github.com/op/go-logging"
	"golang.org/x/sync/errgroup"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/reflection"
	"google.golang.org/grpc/status"
	"net"
	"net/http"
	"strings"
	"sync"
	"time"
)


const KeyName = "server"

var (
	log = logging.MustGetLogger("bistox")
	headerAuthorize = "authorization"
	signInMethod = "/proto_api.BistoxApi/SignIn"
	signUpMethod = "/proto_api.BistoxApi/SignUp"
)


type Mod struct {
	grpcServer *grpc.Server
	config *Config
	httpServer *http.Server
}


type GwRegHandler = func(
	ctx context.Context,
	mux *runtime.ServeMux,
	Addr string,
	option []grpc.DialOption,
) error


type RpcRegHandler = func(*grpc.Server, *runtime.ServeMux)


type ConfigExt = struct {
	RpcRegHandler RpcRegHandler
	GwRegHandlers[] GwRegHandler
	ServerOptions[] grpc.ServerOption
}

type Options = struct {
	Addr string
	GwAddr string
	SecretKey string
	Expire time.Duration
}

type Config struct {
	Enabled bool
	GwEnabled bool
	Ext ConfigExt
	Options Options
}


func GetDefaultConfig() *Config {
	expire, _ := time.ParseDuration("24h")
	return &Config{
		Enabled: false,
		GwEnabled: false,
		Ext: ConfigExt{
			func(server *grpc.Server, mux *runtime.ServeMux) {},
			[]GwRegHandler {},
			[]grpc.ServerOption {},
		},
		Options: Options{
			Addr: ":10050",
			GwAddr: ":10051",
			Expire: expire,
		},
	}
}


func (mod Mod) Cleanup(ready *sync.WaitGroup) {
	log.Info("Stopping gRPC server")
	mod.grpcServer.GracefulStop()
	if mod.config.GwEnabled {
		log.Info("Stopping HTTP (reverse proxy) server")
		mod.httpServer.Shutdown(context.TODO())
	}
	ready.Done()
}


func (Mod) KeyName() string {
	return KeyName
}


func (mod Mod) Enabled() bool {
	return mod.config.Enabled
}


func (mod Mod) Run(ready *sync.WaitGroup, quit chan<- bool) {
	g := new(errgroup.Group)

	gwmux := runtime.NewServeMux(runtime.WithMarshalerOption(
		runtime.MIMEWildcard,
		&runtime.JSONPb{OrigName: true, EmitDefaults: true},
	))

	// http server settings
	mod.httpServer.Addr = mod.config.Options.GwAddr
	// CORS - DEBUG
	mod.httpServer.Handler = allowCORS(gwmux)

	mod.config.Ext.RpcRegHandler(mod.grpcServer, gwmux)

	if mod.config.GwEnabled {
		log.Info("Reverse proxy gateway enabled")
		runtime.HTTPError = DefaultHTTPError
		for _, handler := range mod.config.Ext.GwRegHandlers {
			handler(context.TODO(), gwmux, mod.config.Options.Addr, []grpc.DialOption{grpc.WithInsecure()})
		}
		g.Go(func() error {
			err := mod.httpServer.ListenAndServe()
			log.Criticalf("Failed to listen: %v", err)
			quit <- true
			return err
		})
	}

	g.Go(func() error {
		listener, err := net.Listen("tcp", mod.config.Options.Addr)
		if err != nil {
			log.Criticalf("Failed to listen: %v", err)
			quit <- true
			return err
		}
		err = mod.grpcServer.Serve(listener)
		quit <- true
		return err
	})

	log.Info("Server is active")

	ready.Done()

	g.Wait()
}

func (mod Mod) GetNewToken(accountId string) (string, error) {
	return NewToken(accountId, mod.config.Options.SecretKey, mod.config.Options.Expire)
}


func AuthUnaryServerInterceptor(opts Options) grpc.ServerOption {

	authFunc := func(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (interface{}, error) {
		if info.FullMethod == signInMethod || info.FullMethod == signUpMethod || strings.Contains(info.FullMethod, "Chart") || strings.Contains(info.FullMethod, "GetMarketInfo"){
			return handler(ctx, req)
		} else {
			tokenString, err := AuthFromMD(ctx, "basic")

			if err != nil {
				return nil, err
			}

			token, err := jwt.ParseWithClaims(tokenString, &JWTClaims{}, func(token *jwt.Token) (interface{}, error) {
				return []byte(opts.SecretKey), nil
			})

			if claims, ok := token.Claims.(*JWTClaims); ok && token.Valid {
				log.Debugf("Account ID: %v, expires at: %v", claims.AccountID, claims.StandardClaims.ExpiresAt)

				newCtx := context.WithValue(ctx, "AccountId", claims.AccountID)

				return handler(newCtx, req)
			} else {
				log.Debugf("Token is not valid: %v", err)

				return nil, status.Error(codes.Unauthenticated, "TokenIsInvalid")
			}

			log.Debugf("Token String %s", tokenString)
			log.Debugf("ok %v\n", err)
			return handler(ctx, req)
		}
	};
	return grpc.UnaryInterceptor(grpc_middleware.ChainUnaryServer(authFunc))
}


func AuthFromMD(ctx context.Context, expectedScheme string) (string, error) {
	val := metautils.ExtractIncoming(ctx).Get(headerAuthorize)
	if val == "" {
		return "", status.Errorf(codes.Unauthenticated, "Request unauthenticated with "+expectedScheme)

	}
	splits := strings.SplitN(val, " ", 2)
	if len(splits) < 2 {
		return "", status.Errorf(codes.Unauthenticated, "Bad authorization string")
	}
	if strings.ToLower(splits[0]) != strings.ToLower(expectedScheme) {
		return "", status.Errorf(codes.Unauthenticated, "Request unauthenticated with "+expectedScheme)
	}
	return splits[1], nil
}


func New(config *Config) Mod {
	grpcServer := grpc.NewServer(config.Ext.ServerOptions...)
	reflection.Register(grpcServer)

	return Mod{grpcServer,config, &http.Server{}}
}
