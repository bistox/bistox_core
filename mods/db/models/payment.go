package models

import (
	"encoding/hex"
	"fmt"
	"github.com/go-pg/pg"
	"gitlab.com/bistox/bistox_core/etc"
	proto "gitlab.com/bistox/bistox_proto"
)

type PaymentStatus string
const (
	StatusPreparing PaymentStatus = "Preparing"
	StatusWaiting PaymentStatus = "Waiting"
	StatusPending PaymentStatus = "Pending"
	StatusCompleted PaymentStatus = "Completed"
	StatusFailed PaymentStatus = "Failed"
	StatusNone PaymentStatus = "None"
)


type PaymentDirection string
const (
	DirectionInternal PaymentDirection = "Internal"
	DirectionIncoming PaymentDirection = "Incoming"
	DirectionOutgoing PaymentDirection = "Outgoing"
	DirectionNone PaymentDirection = "None"
)


// Wallet2Wallet
type Payment struct {
	BaseModel
	ID uint64                   `sql:",pk"`
	tableName struct{} 			`sql:"payments,alias:pm"`

	WalletId string				`sql:",notnull,type:uuid"`
	Wallet *Wallet				`pg:"fk:wallet_id"`

	CryptoPayments []*CryptoPayment

	Tx *CryptoTransaction		`pg:"fk:tx_id"`
	TxId []byte					`sql:","`

	Status PaymentStatus 		`sql:",type:payment_status,notnull,"`
	Direction PaymentDirection	`sql:",type:payment_direction,notnull,"`
	CurrencyCode CurrencyEnum	`sql:",type:currency_code,notnull,"`
	Amount int64				`sql:",notnull"`
	Fee int64					`sql:",notnull"`
	BlockHeight uint32			`sql:",notnull,default:0"`

	ExtAddress string			`sql:",type:varchar(42)"`
}


func (p Payment) Prepare(db *pg.DB, force bool) error {
	if err := CreateEnum(
		db, force, "payment_status",
		string(StatusPreparing), string(StatusWaiting),
		string(StatusPending), string(StatusCompleted), string(StatusFailed),
	); err != nil {return  err}

	if err := CreateEnum(
		db, force, "payment_direction",
		string(DirectionInternal), string(DirectionIncoming), string(DirectionOutgoing),
	); err != nil {return  err}

	return nil
}


func (p Payment) Sync(db *pg.DB, force bool) error {
	if _, err := db.Model((*Payment)(nil)).Exec(CreateIndexQuery(false,"tx_id")); err != nil {
		return err
	}
	if _, err := db.Model((*Payment)(nil)).Exec(CreateIndexQuery(false,"block_height", "status")); err != nil {
		return err
	}
	return nil
}


func (p Payment) String() string {
	return fmt.Sprintf(
		"%s[sum=%d, fee=%d, wallet_id=%s, direction=%s, status=%s, txid=%s]",
		p.CurrencyCode, p.Amount, p.Fee, p.WalletId, p.Direction, p.Status,
		hex.EncodeToString(*etc.ReverseBytes(&p.TxId)),
	)
}


func (p *Payment) ToProto() *proto.Payment {
	return &proto.Payment{
		PaymentId: p.ID,
		WalletId: p.WalletId,
		ExtAddress: p.ExtAddress,
		Amount: p.Amount,
		Currency: proto.Currency(proto.Currency_value[string(p.CurrencyCode)]),
		Status: proto.PaymentStatus(proto.PaymentStatus_value[string(p.Status)]),
		Direction: proto.PaymentDirection(proto.PaymentDirection_value[string(p.Direction)]),
		Fee: p.Fee,
	}
}
