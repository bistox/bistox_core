package models

import (
	"fmt"
	"github.com/go-pg/pg"
)


// Stores only last synced block
type CryptoBlock struct {
	tableName struct{}			`sql:"crypto_blocks,alias:bb"`
	CurrencyCode CurrencyEnum	`sql:",pk,unique,type:currency_code,notnull,"`
	Height uint32				`sql:","`
	Hash string					`sql:","`
}


func (CryptoBlock) Prepare(db *pg.DB, force bool) error {
	return nil
}


func (CryptoBlock) Sync(db *pg.DB, force bool) error {
	for _, cc := range []CryptoCurrency{BTC, BCH, ETH, LTC, DASH} {
		db.Insert(&CryptoBlock{CurrencyCode: CurrencyEnum(cc)})
	}
	return nil
}


func (ct *CryptoBlock) String() string {
	return fmt.Sprintf(
		"Block %d[hash=%s, currency=%s]",
		ct.Height, ct.Hash, ct.CurrencyCode,
	)
}
