package bitcoind

import (
	"gitlab.com/bistox/bistox_core/mods/db/models"
	"sync"
)


func (c *Connector) WarmUpCache(full bool) error {
	if full {
	}

	/*if err := c.warmUpTxs(full); err != nil {
		return err
	}*/

	return nil
}


func (c *Connector) warmUpTxs(full bool) error {
	const limit = 100
	var wg sync.WaitGroup
	utxos := make([]*models.Utxo, 1)
	offset := 0

	height := c.bestBlockHeight - uint32(c.config.MinConfirmations)

	for len(utxos) > 0 {
		if err := c.storage.GetUtxos(&utxos, limit, offset, height, full); err != nil {
			return err
		}

		offset += limit

		for _, t := range utxos {
			if t == nil {
				continue
			}
			wg.Add(1)
			go func(t *models.Utxo) {
				defer wg.Done()
				c.addTransaction(t.Address.Address, float64(t.Amount), t.CompactTxOutString())
			}(t)
		}
		wg.Wait()
	}

	log.Info("CryptoCache has been warmed up")

	return nil
}


// Only unspent transactions are accepted
func (c *Connector) addTransaction(address string, amount float64, txCompact string) error {
	lock, err := c.cache.ObtainLock(address)
	if err != nil { return err }
	defer lock.Release(c.ctx)

	log.Debugf("New transaction in cache: %s", txCompact)
	if err := c.cache.InsertUtxo(amount, txCompact); err != nil {
		return err
	}

	return nil
}


func (c *Connector) removeTransaction(address string, txCompact string) error {
	lock, err := c.cache.ObtainLock(address)
	if err != nil { return err }
	defer lock.Release(c.ctx)

	log.Debugf("Del transaction from cache: %s", txCompact)
	if err := c.cache.DeleteUtxo(txCompact); err != nil {
		return err
	}

	return nil
}


