package models

import (
	"github.com/go-pg/pg"
)


type Token struct {
	BaseModel
	tableName struct{} 	`sql:"tokens,alias:t"`
	Token string		`sql:",notnull"`
	Profile *Profile
	ProfileId uint64	`sql:",notnull"`
	Active bool			`sql:",notnull,default:false"`
}


func (t Token) Prepare(db *pg.DB, force bool) error {
	return nil
}


func (t Token) Sync(db *pg.DB, force bool) error {
	return nil
}
