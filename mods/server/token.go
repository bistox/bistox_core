package server

import (
	"github.com/dgrijalva/jwt-go"
	"time"
)


type JWTClaims struct {
	AccountID string `json:"account_id"`
	jwt.StandardClaims
}


func NewToken(accountId string, secretKey string, tokenExpiry time.Duration) (string, error) {

	claims := JWTClaims{
		accountId,
		jwt.StandardClaims{
			ExpiresAt: time.Now().Add(tokenExpiry).Unix(),
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS512, claims)
	return token.SignedString([]byte(secretKey))
}
